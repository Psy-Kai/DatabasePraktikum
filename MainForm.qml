import QtQuick 2.4

Item {
    width: 400
    height: 400
    property alias loader: loader

    Loader {
        id: loader
        anchors.fill: parent
    }

    states: [
        State {
            name: "login"
            PropertyChanges {
                target: loader
                source: "Login.qml"
            }
        },
        State {
            name: "user_profile"
            PropertyChanges {
                target: loader
                source: "UserProfile.qml"
            }
        },
        State {
            name: "babble_search"
            PropertyChanges {
                target: loader
                source: "BabbleSearch.qml"
            }
        },
        State {
            name: "babble_create"
            PropertyChanges {
                target: loader
                source: "BabbleCreate.qml"
            }
        },
        State {
            name: "babble_details"
            PropertyChanges {
                target: loader
                source: "BabbleDetails.qml"
            }
        }
    ]
}
