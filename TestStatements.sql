/* select Babbles from current user */
SELECT Babble.id, BabbleUser.userName, BabbleUser.name, Babble.text, Babble.date,
"" AS Type
FROM Babble
JOIN BabbleUser ON Babble.createdByUser = BabbleUser.id
WHERE BabbleUser.userName = "Psy-Kai"
UNION
/* select Babbles of users the current user follows */
SELECT Babble.id, BabbleUser.userName, BabbleUser.name, Babble.text, Babble.date,
"" AS Type
FROM Babble
JOIN BabbleUser ON Babble.createdByUser = BabbleUser.id
JOIN BabbleUserFollowsBabbleUser ON BabbleUser.id = BabbleUserFollowsBabbleUser.followedBabbleUserId
JOIN BabbleUser AS BabbleUserFollowing ON BabbleUserFollowsBabbleUser.followingBabbleUserId = BabbleUserFollowing.id
LEFT JOIN BabbleUserBlocksBabbleUser ON BabbleUserBlocksBabbleUser.blockedBabbleUserId = BabbleUserFollowing.id AND BabbleUserBlocksBabbleUser.blockingBabbleUserId = BabbleUser.id
WHERE BabbleUserFollowing.userName = "Psy-Kai" AND BabbleUserBlocksBabbleUser.blockedBabbleUserId IS NULL OR BabbleUserBlocksBabbleUser.blockingBabbleUserId IS NULL
UNION
/* select Babbles liked by user */
SELECT Babble.id, BabbleUser.userName, BabbleUser.name, Babble.text, Babble.date,
"likes" AS Type
FROM Babble
JOIN BabbleUser ON Babble.createdByUser = BabbleUser.id
JOIN BabbleUserLikesBabble ON Babble.id = BabbleUserLikesBabble.babbleId
JOIN BabbleUser AS BabbleUserLikes ON BabbleUserLikesBabble.babbleUserId = BabbleUserLikes.id
LEFT JOIN BabbleUserBlocksBabbleUser ON BabbleUserBlocksBabbleUser.blockedBabbleUserId = BabbleUserLikes.id AND BabbleUserBlocksBabbleUser.blockingBabbleUserId = BabbleUser.id
WHERE BabbleUserLikes.userName = "Psy-Kai" AND BabbleUserLikesBabble.isLike = "true" AND BabbleUserBlocksBabbleUser.blockedBabbleUserId IS NULL OR BabbleUserBlocksBabbleUser.blockingBabbleUserId IS NULL
UNION
/* select Babbles disliked by user */
SELECT Babble.id, BabbleUser.userName, BabbleUser.name, Babble.text, Babble.date,
"dislikes" AS Type
FROM Babble
JOIN BabbleUser ON Babble.createdByUser = BabbleUser.id
JOIN BabbleUserLikesBabble ON Babble.id = BabbleUserLikesBabble.babbleId
JOIN BabbleUser AS BabbleUserLikes ON BabbleUserLikesBabble.babbleUserId = BabbleUserLikes.id
LEFT JOIN BabbleUserBlocksBabbleUser ON BabbleUserBlocksBabbleUser.blockedBabbleUserId = BabbleUserLikes.id AND BabbleUserBlocksBabbleUser.blockingBabbleUserId = BabbleUser.id
WHERE BabbleUserLikes.userName = "Psy-Kai" AND BabbleUserLikesBabble.isLike = "false" AND BabbleUserBlocksBabbleUser.blockedBabbleUserId IS NULL OR BabbleUserBlocksBabbleUser.blockingBabbleUserId IS NULL
UNION
/* select Babbles rebabbled by user */
SELECT Babble.id, BabbleUser.userName, BabbleUser.name, Babble.text, Babble.date,
"rebabbled" AS Type
FROM Babble
JOIN BabbleUser ON Babble.createdByUser = BabbleUser.id
JOIN BabbleUserRebabblesBabble ON Babble.id = BabbleUserRebabblesBabble.babbleId
JOIN BabbleUser AS BabbleUserRebabbled ON BabbleUserRebabblesBabble.babbleUserId = BabbleUserRebabbled.id
LEFT JOIN BabbleUserBlocksBabbleUser ON BabbleUserBlocksBabbleUser.blockedBabbleUserId = BabbleUserRebabbled.id AND BabbleUserBlocksBabbleUser.blockingBabbleUserId = BabbleUser.id
WHERE BabbleUserRebabbled.userName = "Psy-Kai" AND BabbleUserBlocksBabbleUser.blockedBabbleUserId IS NULL OR BabbleUserBlocksBabbleUser.blockingBabbleUserId IS NULL