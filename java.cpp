#include "java.h"

#include <QLoggingCategory>

Q_LOGGING_CATEGORY(javaLogCat, "JavaWrapper")

Java::Java()
{}

bool Java::startVm(const QVector<QString> &pathToClasses)
{
    if (m_javaVm != Q_NULLPTR)
        return true;

    JavaVMInitArgs vmArgs;
    QVector<QByteArray> optionsByteArray;
    optionsByteArray << qUtf8Printable(QStringLiteral("-Djava.class.path=%1").arg(
                                           pathToClasses.toList().join(':')));
    JavaVMOption options[optionsByteArray.size()];
    for (auto i = optionsByteArray.size()-1; 0 <= i; --i)
        options[i].optionString = optionsByteArray[i].data();
    vmArgs.options = options;
    vmArgs.nOptions = optionsByteArray.size();
    vmArgs.version = JNI_VERSION_1_8;
    vmArgs.ignoreUnrecognized = false;

    if (JNI_CreateJavaVM(&m_javaVm, reinterpret_cast<void**>(&m_jniEnv), &vmArgs) == JNI_OK) {
        qCInfo(javaLogCat()) << "JVM load succeeded, version:" << m_jniEnv->GetVersion();
        return true;
    }
    return false;
}

bool Java::stopVm()
{
    return m_javaVm->DestroyJavaVM() == JNI_OK;
}

JNIEnv *Java::jniEnv() const
{
    return m_jniEnv;
}

JavaObject::JavaObject(const Java &java) :
    m_java(java)
  , m_class(Q_NULLPTR)
  , m_object(Q_NULLPTR)
{}

JavaObject::JavaObject(const Java &java, jobject object) :
    m_java(java)
  , m_class(object != Q_NULLPTR ? java.jniEnv()->GetObjectClass(object) : Q_NULLPTR)
  , m_object(object)
  , m_objectSmartPointer(QSharedPointer<JObject>::create(java.jniEnv(), m_object))
{}

JavaObject::JavaObject(const JavaObject &other) :
    m_java(other.m_java)
  , m_class(other.m_class)
  , m_object(other.m_object)
  , m_objectSmartPointer(other.m_objectSmartPointer)
{}

JavaObject::JavaObject(JavaObject &&other) :
    m_java(other.m_java)
  , m_class(qMove(other.m_class))
  , m_object(qMove(other.m_object))
  , m_objectSmartPointer(other.m_objectSmartPointer)
{}

JavaObject::~JavaObject()
{}

JavaObject &JavaObject::operator=(const JavaObject &other)
{
    JavaObject tmp(other);
    *this = qMove(tmp);
    return *this;
}

JavaObject &JavaObject::operator=(JavaObject &&other)
{
    if (this != &other) {
        const_cast<Java&>(m_java) = other.m_java;
        m_class = qMove(other.m_class);
        m_object = qMove(other.m_object);
        m_objectSmartPointer = qMove(other.m_objectSmartPointer);
    }

    return *this;
}
bool JavaObject::initJavaObject(const QString &className)
{
    auto *env = m_java.jniEnv();
    m_class = env->FindClass(qPrintable(className));
    if (m_class == Q_NULLPTR) {
        if (env->ExceptionOccurred())
            env->ExceptionDescribe();
        return false;
    }
    m_object = env->NewObject(m_class, env->GetMethodID(m_class, "<init>", "()V"));
    if (m_object == Q_NULLPTR)
        return false;
    m_object = env->NewGlobalRef(m_object);
    if (m_object == Q_NULLPTR)
        return false;
    m_objectSmartPointer = QSharedPointer<JObject>::create(m_java.jniEnv(), m_object);
    return true;
}

bool JavaObject::initJavaObject()
{
    return false;
}

jclass JavaObject::clazz() const
{
    return m_class;
}

jobject JavaObject::object() const
{
    return m_object;
}

JavaObject::JObject::JObject(JNIEnv *jniEnv, jobject object) :
    m_jniEnv(jniEnv)
  , m_object(object)
{}

JavaObject::JObject::~JObject()
{
    if (m_object != Q_NULLPTR)
        m_jniEnv->DeleteGlobalRef(m_object);
}

JavaConnection::JavaConnection(const Java &java, jobject object) :
    JavaObject(java, object)
{
    auto *env = m_java.jniEnv();
    m_closeMethod = env->GetMethodID(m_class, "close", "()V");
    Q_ASSERT(m_closeMethod != Q_NULLPTR);
    m_commitMethod = env->GetMethodID(m_class, "commit", "()V");
    Q_ASSERT(m_closeMethod != Q_NULLPTR);
    m_rollbackMethod = env->GetMethodID(m_class, "rollback", "()V");
    Q_ASSERT(m_closeMethod != Q_NULLPTR);
}

bool JavaConnection::close() const
{
    auto *env = m_java.jniEnv();
    env->CallVoidMethod(m_object, m_closeMethod);
    if (env->ExceptionOccurred()) {
        env->ExceptionClear();
        return false;
    }
    return true;
}

bool JavaConnection::commit() const
{
    auto *env = m_java.jniEnv();
    env->CallVoidMethod(m_object, m_commitMethod);
    if (env->ExceptionOccurred()) {
        env->ExceptionClear();
        return false;
    }
    return true;
}

bool JavaConnection::rollback() const
{
    auto *env = m_java.jniEnv();
    env->CallVoidMethod(m_object, m_rollbackMethod);
    if (env->ExceptionOccurred()) {
        env->ExceptionClear();
        return false;
    }
    return true;
}

JavaSqlDatabase::JavaSqlDatabase(const Java &java) :
    JavaObject(java)
{}

JavaSqlDatabase::~JavaSqlDatabase()
{}

bool JavaSqlDatabase::initJavaObject()
{
    if (!JavaObject::initJavaObject(QStringLiteral("psy/kai/Database")))
        return false;

    auto *env = m_java.jniEnv();
    m_initMethod = env->GetMethodID(m_class, "init", "()Z");
    Q_ASSERT(m_initMethod != Q_NULLPTR);
    m_connectionMethod = env->GetMethodID(m_class, "connection", "()Ljava/sql/Connection;");
    Q_ASSERT(m_connectionMethod != Q_NULLPTR);
    return true;
}

bool JavaSqlDatabase::init() const
{
    return m_java.jniEnv()->CallBooleanMethod(m_object, m_initMethod) == JNI_TRUE;
}

JavaConnection JavaSqlDatabase::connection() const
{
    return JavaConnection(m_java, m_java.jniEnv()->CallObjectMethod(m_object, m_connectionMethod));
}

JavaSqlQuery::JavaSqlQuery(const Java &java, const JavaConnection &connection) :
    JavaObject(java)
  , m_commit(false)
  , m_connection(connection)
{}

JavaSqlQuery::~JavaSqlQuery()
{
    if (m_commit) {
        if (!m_connection.commit())
            m_connection.rollback();
    }
    m_connection.close();
}

bool JavaSqlQuery::initJavaObject()
{
    if (!JavaObject::initJavaObject(QStringLiteral("psy/kai/SqlQuery"), "Ljava/sql/Connection;",
                                    m_connection.object()))
        return false;

    auto *env = m_java.jniEnv();
    m_prepareMethod = env->GetMethodID(m_class, "prepare", "(Ljava/lang/String;)Z");
    Q_ASSERT(m_prepareMethod != Q_NULLPTR);
    m_addBindValueStringMethod = env->GetMethodID(m_class, "addBindValueString", "(Ljava/lang/String;)V");
    Q_ASSERT(m_addBindValueStringMethod != Q_NULLPTR);
    m_addBindValueIntMethod = env->GetMethodID(m_class, "addBindValueInt", "(I)V");
    Q_ASSERT(m_addBindValueIntMethod != Q_NULLPTR);
    m_addBindValueBooleanMethod = env->GetMethodID(m_class, "addBindValueBoolean", "(Z)V");
    Q_ASSERT(m_addBindValueBooleanMethod != Q_NULLPTR);
    m_executeQueryMethod = env->GetMethodID(m_class, "executeQuery", "()Z");
    Q_ASSERT(m_executeQueryMethod != Q_NULLPTR);
    m_executeUpdateMethod = env->GetMethodID(m_class, "executeUpdate", "()I");
    Q_ASSERT(m_executeUpdateMethod != Q_NULLPTR);
    m_nextMethod = env->GetMethodID(m_class, "next", "()Z");
    Q_ASSERT(m_nextMethod != Q_NULLPTR);
    m_valueStringMethod = env->GetMethodID(m_class, "valueString", "(Ljava/lang/String;)Ljava/lang/String;");
    Q_ASSERT(m_valueStringMethod != Q_NULLPTR);
    m_valueIntMethod = env->GetMethodID(m_class, "valueInt", "(Ljava/lang/String;)Ljava/lang/String;");
    Q_ASSERT(m_valueIntMethod != Q_NULLPTR);
    return true;
}

bool JavaSqlQuery::prepare(const QString &statement) const
{
    auto *env = m_java.jniEnv();
    return env->CallBooleanMethod(m_object, m_prepareMethod,
                                  env->NewStringUTF(qUtf8Printable(statement))) == JNI_TRUE;
}

void JavaSqlQuery::addBindValueString(const QString &value) const
{
    auto *env = m_java.jniEnv();
    return env->CallVoidMethod(m_object, m_addBindValueStringMethod,
                               env->NewStringUTF(qUtf8Printable(value)));
}

void JavaSqlQuery::addBindValueInt(int value) const
{
    auto *env = m_java.jniEnv();
    return env->CallVoidMethod(m_object, m_addBindValueIntMethod, value);
}

void JavaSqlQuery::addBindValueBoolean(bool value) const
{
    auto *env = m_java.jniEnv();
    return env->CallVoidMethod(m_object, m_addBindValueBooleanMethod, value);
}

bool JavaSqlQuery::executeQuery() const
{
    return m_java.jniEnv()->CallBooleanMethod(m_object, m_executeQueryMethod) == JNI_TRUE;
}

int JavaSqlQuery::executeUpdate() const
{
    return m_java.jniEnv()->CallIntMethod(m_object, m_executeUpdateMethod);
}

bool JavaSqlQuery::next() const
{
    return m_java.jniEnv()->CallBooleanMethod(m_object, m_nextMethod) == JNI_TRUE;
}

bool JavaSqlQuery::commit()
{
    m_commit = true;
}

QVariant JavaSqlQuery::valueString(const QString &field) const
{
    auto *env = m_java.jniEnv();
    auto jStringResult = env->CallObjectMethod(m_object, m_valueStringMethod,
                                               env->NewStringUTF(qUtf8Printable(field)));
    if (jStringResult == Q_NULLPTR)
        return QString();
    return QLatin1String(env->GetStringUTFChars(static_cast<jstring>(jStringResult), Q_NULLPTR));
}

QVariant JavaSqlQuery::valueInt(const QString &field) const
{
    auto *env = m_java.jniEnv();
    const auto jStringResult = env->CallObjectMethod(m_object, m_valueIntMethod,
                                                     env->NewStringUTF(qUtf8Printable(field)));
    const auto &resultQString = QLatin1String(env->GetStringUTFChars(static_cast<jstring>(jStringResult), Q_NULLPTR));
    if (resultQString.isEmpty())
        return QVariant();
    return resultQString;
}
