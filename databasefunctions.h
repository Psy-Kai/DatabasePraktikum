#pragma once

#include <QImage>
#include <QSqlQuery>
#include <QVariant>
#include <QVector>
#include "java.h"

struct Babble : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint32 id MEMBER id NOTIFY intChanged)
    Q_PROPERTY(QString user MEMBER user NOTIFY stringChanged)
    Q_PROPERTY(QString name MEMBER name NOTIFY stringChanged)
    Q_PROPERTY(QString text MEMBER text NOTIFY stringChanged)
    Q_PROPERTY(qint32 likes MEMBER likes NOTIFY intChanged)
    Q_PROPERTY(qint32 dislikes MEMBER dislikes NOTIFY intChanged)
    Q_PROPERTY(qint32 rebabbles MEMBER rebabbles NOTIFY intChanged)
    Q_PROPERTY(QString timestamp MEMBER timestamp NOTIFY stringChanged)
public:
    explicit Babble();
    Babble(const Babble &other);
    Babble(Babble &&other);
    Babble &operator=(const Babble &other);
    Babble &operator=(Babble &&other);
    ~Babble() Q_DECL_OVERRIDE;
    qint32 id;
    QString user;
    QString name;
    QString text;
    qint32 likes;
    qint32 dislikes;
    qint32 rebabbles;
    QString timestamp;
signals:
    /* just to surpress warnings */
    void intChanged();
    void stringChanged();
};
bool operator==(const Babble &lhs, const Babble &rhs);
bool operator!=(const Babble &lhs, const Babble &rhs);
Q_DECLARE_METATYPE(Babble)

struct Interaction Q_DECL_FINAL
{
public:
    QString user;
    QString type;
    QString timestamp;
    Babble babble;
};

struct UserProfile Q_DECL_FINAL
{
public:
    QString user;
    QString name;
    QString status;
    QImage image;
};

class DatabaseFunctionsInterface
{
public:
    static DatabaseFunctionsInterface &instance();
    virtual ~DatabaseFunctionsInterface() = 0;
    virtual bool init() = 0;
    virtual QVariant likesCount(const QVariant &babbleId) const = 0;
    virtual QVariant dislikesCount(const QVariant &babbleId) const = 0;
    virtual QVariant rebabbledCount(const QVariant &babbleId) const = 0;
    virtual QVector<Interaction> babbles(const QString &user) const = 0;
    virtual QVariant babblesCount(const QString &user) const = 0;
    virtual UserProfile userProfile(const QString &user) const = 0;
    virtual bool checkUserCredentials(const QString &user, const QString &password) const = 0;
    virtual QVector<Babble> searchBabbles(const QString &searchstring) const = 0;
    virtual bool postBabble(const QString &user, const QString &text) const = 0;
    virtual bool deleteBabble(const QVariant &babbleId) const = 0;
    virtual Babble babble(qint32 babbleId) const = 0;
    virtual QVariant likesBabble(const QString &user, qint32 babbleId) const = 0;
    virtual bool likeBabble(const QString &user, qint32 babbleId, bool like, bool update = false) const = 0;
    virtual bool unlikeBabble(const QString &user, qint32 babbleId) const = 0;
    virtual QVariant wasRebabbled(const QString &user, qint32 babbleId) const = 0;
    virtual bool rebabble(const QString &user, qint32 babbleId) const = 0;
    virtual bool unrebabble(const QString &user, qint32 babbleId) const = 0;
    virtual QPair<bool, QVariant> isBlocked(const QString &blockingUser, const QString &blockedUser) const = 0;
    virtual bool block(const QString &user, const QString &userToBlock, const QString &reason) const = 0;
    virtual bool unblock(const QString &user, const QString &userToUnblock) const = 0;
    virtual QVariant follows(const QString &followingUser, const QString &followedUser) const = 0;
    virtual bool follow(const QString &user, const QString &userToFollow) const = 0;
    virtual bool unfollow(const QString &user, const QString &userToUnfollow) const = 0;
};

class SqliteFunctions Q_DECL_FINAL : public DatabaseFunctionsInterface
{
public:
    ~SqliteFunctions() Q_DECL_OVERRIDE;
    bool init() Q_DECL_OVERRIDE;
    QVariant likesCount(const QVariant &babbleId) const Q_DECL_OVERRIDE;
    QVariant dislikesCount(const QVariant &babbleId) const Q_DECL_OVERRIDE;
    QVariant rebabbledCount(const QVariant &babbleId) const Q_DECL_OVERRIDE;
    QVector<Interaction> babbles(const QString &user) const Q_DECL_OVERRIDE;
    QVariant babblesCount(const QString &user) const Q_DECL_OVERRIDE;
    UserProfile userProfile(const QString &user) const Q_DECL_OVERRIDE;
    bool checkUserCredentials(const QString &user, const QString &password) const Q_DECL_OVERRIDE;
    QVector<Babble> searchBabbles(const QString &searchstring) const Q_DECL_OVERRIDE;
    bool postBabble(const QString &user, const QString &text) const Q_DECL_OVERRIDE;
    bool deleteBabble(const QVariant &babbleId) const Q_DECL_OVERRIDE;
    Babble babble(qint32 babbleId) const Q_DECL_OVERRIDE;
    QVariant likesBabble(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    bool likeBabble(const QString &user, qint32 babbleId, bool like, bool update = false) const Q_DECL_OVERRIDE;
    bool unlikeBabble(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    QVariant wasRebabbled(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    bool rebabble(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    bool unrebabble(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    QPair<bool, QVariant> isBlocked(const QString &blockingUser, const QString &blockedUser) const Q_DECL_OVERRIDE;
    bool block(const QString &user, const QString &userToBlock, const QString &reason) const Q_DECL_OVERRIDE;
    bool unblock(const QString &user, const QString &userToUnblock) const Q_DECL_OVERRIDE;
    QVariant follows(const QString &followingUser, const QString &followedUser) const Q_DECL_OVERRIDE;
    bool follow(const QString &user, const QString &userToFollow) const Q_DECL_OVERRIDE;
    bool unfollow(const QString &user, const QString &userToUnfollow) const Q_DECL_OVERRIDE;
private:
    QVariant likesDislikesCount(const QVariant &babbleId, bool like) const;
};

class DB2JavaFunctions Q_DECL_FINAL : public DatabaseFunctionsInterface
{
public:
    explicit DB2JavaFunctions();
    bool init() Q_DECL_OVERRIDE;
    QVariant likesCount(const QVariant &babbleId) const Q_DECL_OVERRIDE;
    QVariant dislikesCount(const QVariant &babbleId) const Q_DECL_OVERRIDE;
    QVariant rebabbledCount(const QVariant &babbleId) const Q_DECL_OVERRIDE;
    QVector<Interaction> babbles(const QString &user) const Q_DECL_OVERRIDE;
    QVariant babblesCount(const QString &user) const Q_DECL_OVERRIDE;
    UserProfile userProfile(const QString &user) const Q_DECL_OVERRIDE;
    bool checkUserCredentials(const QString &user, const QString &password) const Q_DECL_OVERRIDE;
    QVector<Babble> searchBabbles(const QString &searchstring) const Q_DECL_OVERRIDE;
    bool postBabble(const QString &user, const QString &text) const Q_DECL_OVERRIDE;
    bool deleteBabble(const QVariant &babbleId) const Q_DECL_OVERRIDE;
    Babble babble(qint32 babbleId) const Q_DECL_OVERRIDE;
    QVariant likesBabble(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    bool likeBabble(const QString &user, qint32 babbleId, bool like, bool update) const Q_DECL_OVERRIDE;
    bool unlikeBabble(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    QVariant wasRebabbled(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    bool rebabble(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    bool unrebabble(const QString &user, qint32 babbleId) const Q_DECL_OVERRIDE;
    QPair<bool, QVariant> isBlocked(const QString &blockingUser, const QString &blockedUser) const Q_DECL_OVERRIDE;
    bool block(const QString &user, const QString &userToBlock, const QString &reason) const Q_DECL_OVERRIDE;
    bool unblock(const QString &user, const QString &userToUnblock) const Q_DECL_OVERRIDE;
    QVariant follows(const QString &followingUser, const QString &followedUser) const Q_DECL_OVERRIDE;
    bool follow(const QString &user, const QString &userToFollow) const Q_DECL_OVERRIDE;
    bool unfollow(const QString &user, const QString &userToUnfollow) const Q_DECL_OVERRIDE;
private:
    QVariant likesDislikesCount(const QVariant &babbleId, bool like) const;
    Java m_java;
    JavaSqlDatabase m_database;
};
