#pragma once

#include <QSharedPointer>
#include <QVariant>
#include <QVector>
#include <jni.h>

class Java Q_DECL_FINAL
{
public:
    explicit Java();
    bool startVm(const QVector<QString> &pathToClasses=QVector<QString>());
    bool stopVm();
    JNIEnv *jniEnv() const;
private:
    JavaVM *m_javaVm;
    JNIEnv *m_jniEnv;
};

class JavaObject
{
public:
    explicit JavaObject(const Java &java);
    explicit JavaObject(const Java &java, jobject object);
    JavaObject(const JavaObject &other);
    JavaObject(JavaObject &&other);
    virtual ~JavaObject();
    JavaObject &operator=(const JavaObject &other);
    JavaObject &operator=(JavaObject &&other);
    template <typename ...T>
    bool initJavaObject(const QString &className, const QString &constructorParameterString, T ...construcotParameter)
    {
        auto *env = m_java.jniEnv();
        m_class = env->FindClass(qPrintable(className));
        if (m_class == Q_NULLPTR) {
            if (env->ExceptionOccurred())
                env->ExceptionDescribe();
            return false;
        }
        jmethodID constructorId = env->GetMethodID(m_class, "<init>",
                                                   qPrintable(QStringLiteral("(%1)V").arg(constructorParameterString)));
        if (constructorId == Q_NULLPTR) {
            if (env->ExceptionOccurred())
                env->ExceptionDescribe();
            return false;
        }
        m_object = env->NewObject(m_class, constructorId, construcotParameter...);
        if (m_object == Q_NULLPTR)
            return false;
        m_object = env->NewGlobalRef(m_object);
        if (m_object == Q_NULLPTR)
            return false;
        m_objectSmartPointer = QSharedPointer<JObject>::create(m_java.jniEnv(), m_object);
        return true;
    }
    bool initJavaObject(const QString &className);
    virtual bool initJavaObject();
    jclass clazz() const;
    jobject object() const;
protected:
    const Java &m_java;
    jclass m_class;
    jobject m_object;
    struct JObject {
        JNIEnv *m_jniEnv;
        jobject m_object;
        explicit JObject(JNIEnv *jniEnv, jobject object);
        ~JObject();
    };
    friend class JObject;
    QSharedPointer<JObject> m_objectSmartPointer;
};

class JavaConnection : public JavaObject
{
public:
    explicit JavaConnection(const Java &java, jobject object);
    bool close() const;
    bool commit() const;
    bool rollback() const;
private:
    jmethodID m_closeMethod;
    jmethodID m_commitMethod;
    jmethodID m_rollbackMethod;
};

class JavaSqlDatabase : public JavaObject
{
public:
    explicit JavaSqlDatabase(const Java &java);
    ~JavaSqlDatabase() Q_DECL_OVERRIDE;
    bool initJavaObject() Q_DECL_OVERRIDE;
    bool init() const;
    JavaConnection connection() const;
private:
    jmethodID m_initMethod;
    jmethodID m_connectionMethod;
};

class JavaSqlQuery : public JavaObject
{
public:
    explicit JavaSqlQuery(const Java &java, const JavaConnection &connection);
    ~JavaSqlQuery() Q_DECL_OVERRIDE;
    bool initJavaObject() Q_DECL_OVERRIDE;
    bool prepare(const QString &statement) const;
    void addBindValueString(const QString &value) const;
    void addBindValueInt(int value) const;
    void addBindValueBoolean(bool value) const;
    bool executeQuery() const;
    int executeUpdate() const;
    bool next() const;
    bool commit();
    QVariant valueString(const QString &field) const;
    QVariant valueInt(const QString &field) const;
private:
    bool m_commit;
    JavaConnection m_connection;
    jmethodID m_prepareMethod;
    jmethodID m_addBindValueStringMethod;
    jmethodID m_addBindValueIntMethod;
    jmethodID m_addBindValueBooleanMethod;
    jmethodID m_executeQueryMethod;
    jmethodID m_executeUpdateMethod;
    jmethodID m_nextMethod;
    jmethodID m_valueStringMethod;
    jmethodID m_valueIntMethod;
};
