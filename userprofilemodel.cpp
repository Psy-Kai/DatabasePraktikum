#include "userprofilemodel.h"

#include "databasefunctions.h"

UserProfileModel::UserProfileModel() :
    m_blocks(false)
  , m_follows(false)
  , m_blockedByUserOfProfile(false)
{}

void UserProfileModel::queryData()
{
    if (m_userName.isEmpty())
        return;

    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    auto userProfile = databaseFunctions.userProfile(m_userName);
    setName(userProfile.name);
    setStatus(userProfile.status);

    if (m_loggedInUser.isEmpty())
        return;
    if (m_loggedInUser == m_userName)
        return;
    auto isBlocked = databaseFunctions.isBlocked(m_loggedInUser, m_userName);
    if (isBlocked.first)
        setBlocks(isBlocked.second.isValid());
    auto follows = databaseFunctions.follows(m_loggedInUser, m_userName);
    if (follows.isValid())
        setFollows(follows.value<bool>());
    isBlocked = databaseFunctions.isBlocked(m_userName, m_loggedInUser);
    if (isBlocked.first) {
        setBlockedByUserOfProfile(isBlocked.second.isValid());
        setBlockReason(isBlocked.second.value<QString>());
    }
}

QString UserProfileModel::userName() const
{
    return m_userName;
}

QString UserProfileModel::name() const
{
    return m_name;
}

QString UserProfileModel::status() const
{
    return m_status;
}

QString UserProfileModel::imageSource() const
{
    return m_imageSource;
}

bool UserProfileModel::blocks() const
{
    return m_blocks;
}

bool UserProfileModel::follows() const
{
    return m_follows;
}

QString UserProfileModel::loggedInUser() const
{
    return m_loggedInUser;
}

bool UserProfileModel::blockedByUserOfProfile() const
{
    return m_blockedByUserOfProfile;
}

QString UserProfileModel::blockReason() const
{
    return m_blockReason;
}

void UserProfileModel::setUserName(QString userName)
{
    if (m_userName == userName)
        return;

    m_userName = userName;
    queryData();
    emit userNameChanged(m_userName);
}

void UserProfileModel::setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(m_name);
}

void UserProfileModel::setStatus(QString status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(m_status);
}

void UserProfileModel::setBlocks(bool blocked)
{
    if (m_blocks == blocked)
        return;

    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    if (blocked)
        databaseFunctions.block(m_loggedInUser, m_userName, QStringLiteral("dummy"));
    else
        databaseFunctions.unblock(m_loggedInUser, m_userName);
    m_blocks = blocked;
    emit blocksChanged(m_blocks);
}

void UserProfileModel::setFollows(bool follow)
{
    if (m_follows == follow)
        return;

    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    if (follow)
        databaseFunctions.follow(m_loggedInUser, m_userName);
    else
        databaseFunctions.unfollow(m_loggedInUser, m_userName);
    m_follows = follow;
    emit followsChanged(m_follows);
}

void UserProfileModel::setLoggedInUser(QString loggedInUser)
{
    if (m_loggedInUser == loggedInUser)
        return;

    m_loggedInUser = loggedInUser;
    queryData();
    emit loggedInUserChanged(m_loggedInUser);
}

void UserProfileModel::setBlockedByUserOfProfile(bool blocked)
{
    if (m_blockedByUserOfProfile == blocked)
        return;

    m_blockedByUserOfProfile = blocked;
    emit blockedByUserOfProfileChanged(m_blockedByUserOfProfile);
}

void UserProfileModel::setBlockReason(QString blockReason)
{
    if (m_blockReason == blockReason)
        return;

    m_blockReason = blockReason;
    emit blockReasonChanged(m_blockReason);
}
