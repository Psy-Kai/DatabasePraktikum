#pragma once

#include <QObject>
#include <QVariant>

class Babble;
class Main : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currentlyLoggedInUser READ currentlyLoggedInUser
               WRITE setCurrentlyLoggedInUser NOTIFY currentlyLoggedInUserChanged)
public:
    explicit Main();
    QString currentlyLoggedInUser() const;
    Q_INVOKABLE bool checkUserCredentials(const QString &user, const QString &password) const;
    Q_INVOKABLE void postBabble(const QString &text) const;
    Q_INVOKABLE void deleteBabble(qint32 babbleId) const;
public slots:
    void setCurrentlyLoggedInUser(QString currentlyLoggedInUser);
signals:
    void currentlyLoggedInUserChanged(QString currentlyLoggedInUser);
private:
    QString m_currentlyLoggedInUser;
};
