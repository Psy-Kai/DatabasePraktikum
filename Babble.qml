import QtQuick 2.4

Item {
    id: root

    property int id: -1
    property string user: "user"
    property string name: "name"
    property string text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
    property int likes: 0
    property int dislikes: 0
    property int rebabbles: 0
    property string timestamp: (new Date).toLocaleString(Qt.locale("de_DE"), "dd. MMMM yyyy hh:mm")

    signal userNameClicked
    signal textClicked
    width: 600

    height: form.labelTimestamp.y + form.labelTimestamp.height

    BabbleForm {
        id: form
        anchors.fill: parent

        labelName.text: root.name
        labelUser.text: root.user
        textBabbleText.text: root.text
        labelLikesValue.text: root.likes
        labelDislikesValue.text: root.dislikes
        labelRebabblesValue.text: root.rebabbles
        labelTimestamp.text: root.timestamp

        mouseAreaNameUser.onClicked: root.userNameClicked()
        mouseAreaBabbleText.onClicked: root.textClicked()
    }
}
