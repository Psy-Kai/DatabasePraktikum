import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
    id: root
    width: 400
    height: 400
    property alias textFieldSearchbar: textFieldSearchbar
    property alias buttonSearchBabble: buttonSearchBabble
    property alias listView: listView

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        RowLayout {
            id: row
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            spacing: 15
            Layout.maximumWidth: 800
            Layout.fillWidth: true

            TextField {
                id: textFieldSearchbar
                height: 35
                text: qsTr("Text Field")
                Layout.fillWidth: true
            }

            Button {
                id: buttonSearchBabble
                text: qsTr("Search babble")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                height: parent.height
            }
        }

        Row {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            spacing: 5
            Layout.maximumWidth: 800
            Layout.fillWidth: true
            Label {
                id: labelSeachResultFor
                text: qsTr("Search result for:")
            }

            Label {
                id: labelSearchResultValue
                text: textFieldSearchbar.text
            }
        }

        ListView {
            id: listView
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.maximumWidth: 800
            Layout.fillHeight: true
            Layout.fillWidth: true
            boundsBehavior: Flickable.StopAtBounds
        }
    }
}
