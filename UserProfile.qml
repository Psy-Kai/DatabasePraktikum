import QtQuick 2.4
import kd.components 1.0

Item {
    id: root

    property string user

    signal showBabble(var babbleId)
    signal searchBabble()
    signal createBabble()

    UserProfileModel {
        id: model
        loggedInUser: currentlyLoggedInUser
        userName: user
    }

    UserProfileForm {
        id: form
        anchors.fill: parent

        labelUserValue.text: model.userName
        labelNameValue.text: model.name
        labelStatusValue.text: model.status
        image.source: model.imageSource.isEmpty ? "qrc:/ProfilePicPlaceholder.png" :
                                                  model.imageSource

        buttonSearchBabble.onClicked: root.searchBabble()
        buttonNewBabble.onClicked: root.createBabble()

        buttonBlock.onClicked: {
            model.blocks = !model.blocks;
            timelineModel.update();
        }
        buttonFollow.onClicked: model.follows = !model.follows;

        buttonBlock.states: [
            State {
                name: "blocks"
                when: model.blocks
                PropertyChanges {
                    target: form.buttonBlock
                    text: qsTr("Unblock")
                }
            }
        ]
        buttonFollow.states: [
            State {
                name: "follows"
                when: model.follows
                PropertyChanges {
                    target: form.buttonFollow
                    text: qsTr("Unfollow")
                }
            }
        ]

        listView.delegate: TimelineDelegate {
            anchors.left: parent.left
            anchors.right: parent.right

            state: model.InteractionType
            label.text: model.InteractionUser
            babble.id: model.Id
            babble.user: model.User
            babble.name: model.Name
            babble.text: model.Text
            babble.likes: model.Likes
            babble.dislikes: model.Dislikes
            babble.rebabbles: model.Rebabbles
            babble.timestamp: model.Timestamp

            onUserClicked: root.user = babble.user;
            onTextClicked: root.showBabble(babble.id);
        }

        listView.model: TimelineModel {
            id: timelineModel
            user: root.user
        }

        states: [
            State {
                name: "showLoggedInUser"
                when: model.userName === model.loggedInUser
                PropertyChanges {
                    target: form.buttonBlock
                    visible: false
                }
                PropertyChanges {
                    target: form.buttonFollow
                    visible: false
                }
            },
            State {
                name: "blockedByShowedUser"
                when: model.blockedByUserOfProfile
                PropertyChanges {
                    target: form.buttonFollow
                    visible: false
                }
                PropertyChanges {
                    target: form.listView
                    visible: false
                }
                PropertyChanges {
                    target: form.labelBlockingReason
                    visible: true
                    text: model.blockReason
                }
            }
        ]
    }
}
