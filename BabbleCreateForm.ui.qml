import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

Item {
    id: root
    width: 400
    height: 400
    property alias label: label
    property alias textAreaBabbleText: textAreaBabbleText
    property alias buttonPostBabble: buttonPostBabble

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        TextArea {
            id: textAreaBabbleText
            text: qsTr("")
            Layout.maximumWidth: 800
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        RowLayout {
            id: rowLayout
            width: 100
            height: 100
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.maximumWidth: 800
            Layout.fillWidth: true

            Label {
                id: label
                color: "#ff0000"
                text: qsTr("Babble is too long, only 280 letters are allowed!")
                visible: false
            }

            Button {
                id: buttonPostBabble
                text: qsTr("Post Babble")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
        }
    }
}
