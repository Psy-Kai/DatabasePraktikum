#include "searchmodel.h"

#include <QMetaEnum>

SearchModel::SearchModel() :
    m_databaseFunctions(DatabaseFunctionsInterface::instance())
{}

SearchModel::~SearchModel()
{}

QString SearchModel::searchString() const
{
    return m_searchString;
}

int SearchModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return -1;

    return m_data.size();
}

QVariant SearchModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (m_data.size() <= index.row())
        return QVariant();

    const auto &babble = m_data[index.row()];
    switch (role) {
        case Id: return babble.id;
        case User: return babble.user;
        case Name: return babble.name;
        case Text: return babble.text;
        case Likes: return babble.likes;
        case Dislikes: return babble.dislikes;
        case Rebabbles: return babble.rebabbles;
        case Timestamp: return babble.timestamp;
        default: return QVariant();
    }
}

QHash<int, QByteArray> SearchModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    const auto &metaEnum = QMetaEnum::fromType<Role>();

    for (auto i = metaEnum.keyCount()-1; 0 <= i; --i)
        roles.insert(metaEnum.value(i), metaEnum.key(i));

    return roles;
}

void SearchModel::setSearchString(QString searchstring)
{
    if (m_searchString == searchstring)
        return;

    beginResetModel();
    m_searchString.clear();
    endResetModel();
    m_searchString = searchstring;
    m_data = m_databaseFunctions.searchBabbles(m_searchString);
    beginInsertRows(QModelIndex(), 0, rowCount(QModelIndex())-1);
    endInsertRows();
    emit searchStringChanged(m_searchString);
}
