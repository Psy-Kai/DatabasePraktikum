import qbs

Project {
    minimumQbsVersion: "1.7.1"

    CppApplication {
        Depends {
            name: "Qt"
            submodules: [
                "core",
                "sql",
                "quick",
                "concurrent"
            ]
        }

        // Additional import path used to resolve QML modules in Qt Creator's code model
        property pathList qmlImportPaths: []

        cpp.cxxLanguageVersion: "c++11"

        cpp.defines: [
            // The following define makes your compiler emit warnings if you use
            // any feature of Qt which as been marked deprecated (the exact warnings
            // depend on your compiler). Please consult the documentation of the
            // deprecated API in order to know how to port your code away from it.
            "QT_DEPRECATED_WARNINGS",
            "DATABASETYPE=DB2JavaFunctions",
            "DB2DRIVERPATH=\"/home/psy-kai/Develop/jdbc_sqlj/db2_db2driver_for_jdbc_sqlj/db2jcc.jar\"",

            // You can also make your code fail to compile if you use deprecated APIs.
            // In order to do so, uncomment the following line.
            // You can also select to disable deprecated APIs only up to a certain version of Qt.
            //"QT_DISABLE_DEPRECATED_BEFORE=0x060000" // disables all the APIs deprecated before Qt 6.0.0
        ]

        cpp.includePaths: [
            "/usr/lib/jvm/java-8-openjdk/include"
        ]

        Properties {
            condition: qbs.targetOS.contains("linux")
            cpp.includePaths: outer.concat(
                                  "/usr/lib/jvm/java-8-openjdk/include/linux"
                                  )
        }
        Properties {
            condition: qbs.targetOS.contains("windows")
            cpp.includePaths: outer.concat(
                                  "/usr/lib/jvm/java-8-openjdk/include/windows"
                                  )
        }

        cpp.libraryPaths: [
            "/usr/lib/jvm/java-8-openjdk/jre/lib/amd64/server",
        ]
        cpp.dynamicLibraries: [
            "jvm"
        ]

        files: [
            "TestStatements.sql",
            "databasefunctions.cpp",
            "databasefunctions.h",
            "java.cpp",
            "java.h",
            "main.cpp",
            "main.h",
            "qml.qrc",
        ]

        Group {
            name: "models"
            files: [
                "babbledetailsmodel.cpp",
                "babbledetailsmodel.h",
                "searchmodel.cpp",
                "searchmodel.h",
                "timelinemodel.cpp",
                "timelinemodel.h",
                "userprofilemodel.cpp",
                "userprofilemodel.h",
            ]
        }

        Group {     // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install: true
        }
    }
}
