import QtQuick 2.4
import QtQuick.Controls 2.3

Item {
    id: root
    width: 400
    height: 400
    property alias mouseAreaBabbleText: mouseAreaBabbleText
    property alias mouseAreaNameUser: mouseAreaNameUser
    property alias labelUser: labelUser
    property alias labelName: labelName
    property alias labelRebabblesValue: labelRebabblesValue
    property alias labelDislikesValue: labelDislikesValue
    property alias labelLikesValue: labelLikesValue
    property alias labelTimestamp: labelTimestamp
    property alias textBabbleText: textBabbleText

    Row {
        id: rowNameUser
        anchors.left: parent.left
        anchors.top: parent.top

        Label {
            id: labelName
            text: qsTr("Label")
        }

        Label {
            id: labelNameAUser
            text: qsTr("@")
        }

        Label {
            id: labelUser
            text: qsTr("Label")
        }
    }

    MouseArea {
        id: mouseAreaNameUser
        z: 1
        anchors.top: rowNameUser.top
        anchors.bottom: rowNameUser.bottom
        anchors.left: rowNameUser.left
        anchors.right: rowNameUser.right
    }

    Text {
        id: textBabbleText
        anchors.left: parent.left
        wrapMode: Text.WordWrap
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 50
    }

    MouseArea {
        id: mouseAreaBabbleText
        z: 2
        anchors.right: textBabbleText.right
        anchors.left: textBabbleText.left
        anchors.bottom: textBabbleText.bottom
        anchors.top: textBabbleText.top
    }

    Row {
        id: rowLikesDislikesRebabbles
        anchors.topMargin: 15
        anchors.top: textBabbleText.bottom
        anchors.right: parent.right

        Label {
            id: labelLikesValue
            text: qsTr("0")
        }

        Label {
            id: labelLikes
            text: qsTr(" Likes / ")
        }

        Label {
            id: labelDislikesValue
            text: qsTr("0")
        }

        Label {
            id: labelDislikes
            text: qsTr(" Dislikes / ")
        }

        Label {
            id: labelRebabblesValue
            text: qsTr("0")
        }

        Label {
            id: labelRebabbles
            text: qsTr(" Rebabbles")
        }
    }

    Label {
        id: labelTimestamp
        text: qsTr("Label")
        anchors.top: rowLikesDislikesRebabbles.bottom
        anchors.topMargin: 0
        anchors.right: parent.right
    }
}
