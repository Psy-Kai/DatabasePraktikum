#pragma once

#include <QAbstractListModel>
#include "databasefunctions.h"

class DatabaseFunctionsInterface;
class TimelineModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString loggedInUser READ loggedInUser WRITE setLoggedInUser NOTIFY loggedInUserChanged)
    Q_PROPERTY(QString user READ user WRITE setUser NOTIFY userChanged)
public:
    enum Role {
        InteractionUser = Qt::UserRole + 1,
        InteractionType,
        InteractionTimestamp,
        Id,
        User,
        Name,
        Text,
        Likes,
        Dislikes,
        Rebabbles,
        Timestamp
    };
    Q_ENUM(Role)
    explicit TimelineModel();
    Q_INVOKABLE void update();
    ~TimelineModel() Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    QString user() const;
    QString loggedInUser() const;
public slots:
    void setUser(QString user);
    void setLoggedInUser(QString loggedInUser);
signals:
    void userChanged(QString user);
    void loggedInUserChanged(QString loggedInUser);
private:
    QString m_user;
    DatabaseFunctionsInterface &m_databaseFunctions;
    QString m_loggedInUser;
    QVector<Interaction> m_data;
};
