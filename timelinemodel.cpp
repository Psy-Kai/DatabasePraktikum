#include "timelinemodel.h"

#include <QtConcurrent>
#include <QFutureWatcher>
#include <QMetaEnum>
#include <QMutexLocker>

TimelineModel::TimelineModel() :
    m_databaseFunctions(DatabaseFunctionsInterface::instance())
{}

void TimelineModel::update()
{
    const auto user = m_user;
    beginResetModel();
    m_user.clear();
    m_data.clear();
    endResetModel();
    m_user = user;
    m_data = m_databaseFunctions.babbles(m_user);
    beginInsertRows(QModelIndex(), 0, rowCount(QModelIndex())-1);
    endInsertRows();
}

TimelineModel::~TimelineModel()
{}

int TimelineModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return -1;

    return m_data.size();
}

QVariant TimelineModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (m_data.size() <= index.row())
        return QVariant();

    const auto &item = m_data[index.row()];
    const auto &babble = item.babble;
    switch (role) {
        case InteractionUser: return item.user;
        case InteractionType: return item.type;
        case InteractionTimestamp: return item.timestamp;
        case Id: return babble.id;
        case User: return babble.user;
        case Name: return babble.name;
        case Text: return babble.text;
        case Likes: return babble.likes;
        case Dislikes: return babble.dislikes;
        case Rebabbles: return babble.rebabbles;
        case Timestamp: return babble.timestamp;
        default: return QVariant();
    }
}

QHash<int, QByteArray> TimelineModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    const auto &metaEnum = QMetaEnum::fromType<Role>();

    for (auto i = metaEnum.keyCount()-1; 0 <= i; --i)
        roles.insert(metaEnum.value(i), metaEnum.key(i));

    return roles;
}

QString TimelineModel::user() const
{
    return m_user;
}

QString TimelineModel::loggedInUser() const
{
    return m_loggedInUser;
}

void TimelineModel::setUser(QString user)
{
    if (m_user == user)
        return;

    m_user = user;
    update();
    emit userChanged(m_user);
}

void TimelineModel::setLoggedInUser(QString loggedInUser)
{
    if (m_loggedInUser == loggedInUser)
        return;

    m_loggedInUser = loggedInUser;
    emit loggedInUserChanged(m_loggedInUser);
}
