import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Rectangle {
    id: root

    property alias babble: babble
    property alias label: label

    signal userClicked
    signal textClicked

    height: babble.y + babble.height + 5
    border.width: 1
    radius: 5
    
    Row {
        id: rowInteraction
        x: 5
        y: 5
        spacing: 3
        visible: false
        Label {
            id: label
            font.italic: true
            font.bold: true
        }
        Label {
            id: labelInteractionText
            font.italic: true
            font.bold: true
        }
    }
    
    Babble {
        id: babble
        anchors.top: rowInteraction.top
        anchors.leftMargin: 5
        anchors.left: parent.left
        anchors.rightMargin: 5
        anchors.right: parent.right
        onUserNameClicked: root.userClicked()
        onTextClicked: root.textClicked()
    }

    states: [
        State {
            name: "likes"
            PropertyChanges {
                target: rowInteraction
                visible: true
            }
            PropertyChanges {
                target: labelInteractionText
                text: "likes"
            }
            PropertyChanges {
                target: babble
                anchors.top: rowInteraction.bottom
                anchors.topMargin: 7
            }
        },
        State {
            name: "dislikes"
            PropertyChanges {
                target: rowInteraction
                visible: true
            }
            PropertyChanges {
                target: labelInteractionText
                text: "dislikes"
            }
            PropertyChanges {
                target: babble
                anchors.top: rowInteraction.bottom
                anchors.topMargin: 7
            }
        },
        State {
            name: "rebabbled"
            PropertyChanges {
                target: rowInteraction
                visible: true
            }
            PropertyChanges {
                target: labelInteractionText
                text: "rebabbled"
            }
            PropertyChanges {
                target: babble
                anchors.top: rowInteraction.bottom
                anchors.topMargin: 7
            }
        }
    ]
}
