import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
    id: root
    width: 400
    height: 400
    property alias labelInvalidCredentials: labelInvalidCredentials
    property alias buttonLogin: buttonLogin
    property alias textFieldPassword: textFieldPassword
    property alias textFieldUsername: textFieldUsername

    ColumnLayout {
        id: column
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Label {
            id: labelUsername
            color: "#000000"
            text: qsTr("Username")
        }

        TextField {
            id: textFieldUsername
        }

        Label {
            id: labelPassword
            color: "#000000"
            text: qsTr("Password")
        }

        TextField {
            id: textFieldPassword
            echoMode: TextInput.Password
        }

        Button {
            id: buttonLogin
            text: qsTr("Login")
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }
    }

    Label {
        id: labelInvalidCredentials
        width: 207
        color: "#ff1212"
        text: qsTr("Username or Password not valid!!")
        visible: false
        anchors.top: column.bottom
        anchors.topMargin: 5
        anchors.left: column.left
    }
}
