import QtQuick 2.4
import kd.components 1.0

Item {
    id: root

    property int babbleId: -1

    signal userClicked(var user)
    signal deleteBabble(var babble)

    onBabbleIdChanged: model.queryBabble(root.babbleId)

    BabbleDetailsModel {
        id: model
        loggedInUser: currentlyLoggedInUser
    }

    BabbleDetailsForm {
        id: form

        anchors.fill: parent

        babble.id: model.babble.id
        babble.user: model.babble.user
        babble.name: model.babble.name
        babble.text: model.babble.text
        babble.likes: model.babble.likes
        babble.dislikes: model.babble.dislikes
        babble.rebabbles: model.babble.rebabbles
        babble.timestamp: model.babble.timestamp
        buttonLike.checked: model.disLikes === BabbleDetailsModel.Like
        buttonDislike.checked: model.disLikes === BabbleDetailsModel.Dislike
        buttonRebabble.checked: model.rebabbles
        buttonDelete.visible: model.loggedInUser === model.babble.user

        buttonLike.onClicked: {
            switch (model.disLikes) {
                case BabbleDetailsModel.None:
                case BabbleDetailsModel.Dislike:
                    model.disLikes = BabbleDetailsModel.Like;
                    break;
                case BabbleDetailsModel.Like:
                    model.disLikes = BabbleDetailsModel.None;
                    break;
                default:
                    break;
            }
        }

        buttonDislike.onClicked: {
            switch (model.disLikes) {
                case BabbleDetailsModel.None:
                case BabbleDetailsModel.Like:
                    model.disLikes = BabbleDetailsModel.Dislike;
                    break;
                case BabbleDetailsModel.Dislike:
                    model.disLikes = BabbleDetailsModel.None;
                    break;
                default:
                    break;
            }
        }

        buttonRebabble.onClicked: model.rebabbles = !model.rebabbles;
        buttonDelete.onClicked: root.deleteBabble(model.babble.id);
        babble.onUserNameClicked: root.userClicked(babble.user);
    }
}
