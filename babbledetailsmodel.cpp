#include "babbledetailsmodel.h"

BabbleDetailsModel::BabbleDetailsModel() :
    QObject()
  , m_disLikes(None)
  , m_rebabbles(false)
{}

void BabbleDetailsModel::queryBabble(qint32 babbleId)
{
    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    auto babble = databaseFunctions.babble(babbleId);
    setBabble(&babble);
    const auto &likes = databaseFunctions.likesBabble(m_loggedInUser, babbleId);
    if (likes.isValid()) {
        const auto &likesValue = likes.value<bool>();
        if (likesValue)
            m_disLikes = Like;
        else
            m_disLikes = Dislike;
    } else {
        m_disLikes = None;
    }
    const auto &rebabbled = databaseFunctions.wasRebabbled(m_loggedInUser, babbleId);
    if (rebabbled.isValid())
        m_rebabbles = rebabbled.value<bool>();
    else
        m_rebabbles = rebabbled.value<bool>();
    emit disLikesChanged(m_disLikes);
    emit rebabblesChanged(m_rebabbles);
}

const Babble *BabbleDetailsModel::babble() const
{
    return &m_babble;
}

Babble *BabbleDetailsModel::babble()
{
    return &m_babble;
}

bool BabbleDetailsModel::rebabbles() const
{
    return m_rebabbles;
}

QString BabbleDetailsModel::loggedInUser() const
{
    return m_loggedInUser;
}

BabbleDetailsModel::DisLikes BabbleDetailsModel::disLikes() const
{
    return m_disLikes;
}

void BabbleDetailsModel::setBabble(Babble *babble)
{
    if (&m_babble == babble)
        return;

    m_babble = *babble;
    emit babbleChanged(&m_babble);
}

void BabbleDetailsModel::setRebabbles(bool rebabbles)
{
    if (m_rebabbles == rebabbles)
        return;

    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    if (rebabbles) {
        databaseFunctions.rebabble(m_loggedInUser, m_babble.id);
        ++m_babble.rebabbles;
    } else {
        databaseFunctions.unrebabble(m_loggedInUser, m_babble.id);
        --m_babble.rebabbles;
    }
    m_rebabbles = rebabbles;
    emit rebabblesChanged(m_rebabbles);
    emit babbleChanged(&m_babble);
}

void BabbleDetailsModel::setLoggedInUser(QString loggedInUser)
{
    if (m_loggedInUser == loggedInUser)
        return;

    m_loggedInUser = loggedInUser;
    emit loggedInUserChanged(m_loggedInUser);
}

void BabbleDetailsModel::setDisLikes(BabbleDetailsModel::DisLikes disLikes)
{
    if (m_disLikes == disLikes)
        return;

    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    switch (disLikes) {
        case None:
            databaseFunctions.unlikeBabble(m_loggedInUser, m_babble.id);
            if (m_disLikes == Like)
                --m_babble.likes;
            else
                --m_babble.dislikes;
            break;
        case Like:
            if (m_disLikes == Dislike) {
                databaseFunctions.likeBabble(m_loggedInUser, m_babble.id, true, true);
                --m_babble.dislikes;
            } else {
                databaseFunctions.likeBabble(m_loggedInUser, m_babble.id, true);
            }
            ++m_babble.likes;
            break;
        case Dislike:
            if (m_disLikes == Like) {
                databaseFunctions.likeBabble(m_loggedInUser, m_babble.id, false, true);
                --m_babble.likes;
            } else {
                databaseFunctions.likeBabble(m_loggedInUser, m_babble.id, false);
            }
            ++m_babble.dislikes;
            break;
        default:
            return;
    }
    m_disLikes = disLikes;
    emit disLikesChanged(m_disLikes);
    emit babbleChanged(&m_babble);
}
