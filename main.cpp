#include "main.h"

#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "babbledetailsmodel.h"
#include "searchmodel.h"
#include "timelinemodel.h"
#include "userprofilemodel.h"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    if (!databaseFunctions.init())
        qFatal("Could not open database");

    qmlRegisterType<BabbleDetailsModel>("kd.components", 1, 0, "BabbleDetailsModel");
    qmlRegisterType<SearchModel>("kd.components", 1, 0, "SearchModel");
    qmlRegisterType<TimelineModel>("kd.components", 1, 0, "TimelineModel");
    qmlRegisterType<UserProfileModel>("kd.components", 1, 0, "UserProfileModel");

    Main m;
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(&m);
    engine.load(QUrl(QStringLiteral("qrc:/Main.qml")));
    if (engine.rootObjects().isEmpty())
        qFatal("Could not load root qmlfile");

    return app.exec();
}

Main::Main()
{}

QString Main::currentlyLoggedInUser() const
{
    return m_currentlyLoggedInUser;
}

bool Main::checkUserCredentials(const QString &user, const QString &password) const
{
    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    return databaseFunctions.checkUserCredentials(user, password);
}

void Main::postBabble(const QString &text) const
{
    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    databaseFunctions.postBabble(m_currentlyLoggedInUser, text);
}

void Main::deleteBabble(qint32 babbleId) const
{
    auto &databaseFunctions = DatabaseFunctionsInterface::instance();
    databaseFunctions.deleteBabble(babbleId);
}

void Main::setCurrentlyLoggedInUser(QString currentlyLoggedInUser)
{
    if (m_currentlyLoggedInUser == currentlyLoggedInUser)
        return;

    m_currentlyLoggedInUser = currentlyLoggedInUser;
    emit currentlyLoggedInUserChanged(m_currentlyLoggedInUser);
}
