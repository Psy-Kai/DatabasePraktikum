import QtQuick 2.10
import QtQuick.Window 2.10

Window {
    id: root

    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    function login(user) {
        currentlyLoggedInUser = user;
        form.uglyBuffer = user;
        form.state = "user_profile";
    }
    function logout() {
        form.state = "login";
        currentlyLoggedInUser = "";
    }

    MainForm {
        id: form
        anchors.fill: parent

        property var uglyBuffer

        loader.onLoaded: {
            var item = loader.item;
            if (state === "login") {
                item.onLogin.connect(login);
            } else if (state === "user_profile") {
                item.user = form.uglyBuffer;
                item.onSearchBabble.connect(function () { form.state = "babble_search"; });
                item.onCreateBabble.connect(function () { form.state = "babble_create"; });
                item.onShowBabble.connect(function (babbleId) {
                    form.uglyBuffer = babbleId;
                    form.state = "babble_details";
                });
            } else if (state === "babble_details") {
                item.babbleId = form.uglyBuffer;
                item.onUserClicked.connect(function(user) {
                    form.uglyBuffer = user;
                    form.state = "user_profile";
                });
                item.onDeleteBabble.connect(function (babbleId) {
                    deleteBabble(babbleId);
                    form.uglyBuffer = currentlyLoggedInUser;
                    form.state = "user_profile";
                });
            } else if (state === "babble_create") {
                item.onPostBabble.connect(function (text) {
                    postBabble(text);
                    form.uglyBuffer = currentlyLoggedInUser;
                    form.state = "user_profile";
                });
            } else if (state === "babble_search") {
                item.onUserClicked.connect(function (user) {
                    form.uglyBuffer = user;
                    form.state = "user_profile";
                });
                item.onShowBabble.connect(function (babbleId) {
                    form.uglyBuffer = babbleId;
                    form.state = "babble_details";
                });
            }
        }
    }

    Component.onCompleted: {
//        form.state = "login"
        login("Psy-Kai");
    }
}
