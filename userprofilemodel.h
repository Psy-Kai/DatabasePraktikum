#pragma once

#include <QImage>

class UserProfileModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString loggedInUser READ loggedInUser WRITE setLoggedInUser NOTIFY loggedInUserChanged)
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(QString imageSource READ imageSource NOTIFY imageChanged)
    Q_PROPERTY(bool blocks READ blocks WRITE setBlocks NOTIFY blocksChanged)
    Q_PROPERTY(bool follows READ follows WRITE setFollows NOTIFY followsChanged)
    Q_PROPERTY(bool blockedByUserOfProfile READ blockedByUserOfProfile
               WRITE setBlockedByUserOfProfile NOTIFY blockedByUserOfProfileChanged)
    Q_PROPERTY(QString blockReason READ blockReason WRITE setBlockReason NOTIFY blockReasonChanged)
public:
    explicit UserProfileModel();
    Q_INVOKABLE void queryData();
    QString userName() const;
    QString name() const;
    QString status() const;
    QString imageSource() const;
    bool blocks() const;
    bool follows() const;
    QString loggedInUser() const;
    bool blockedByUserOfProfile() const;
    QString blockReason() const;
public slots:
    void setUserName(QString userName);
    void setName(QString name);
    void setStatus(QString status);
    void setBlocks(bool blocks);
    void setFollows(bool follows);
    void setLoggedInUser(QString loggedInUser);
    void setBlockedByUserOfProfile(bool blockedByUserOfProfile);
    void setBlockReason(QString blockReason);
signals:
    void userNameChanged(QString userName);
    void nameChanged(QString name);
    void statusChanged(QString status);
    void imageChanged(QString imageSource);
    void blocksChanged(bool blocks);
    void followsChanged(bool follows);
    void loggedInUserChanged(QString loggedInUser);
    void blockedByUserOfProfileChanged(bool blockedByUserOfProfile);
    void blockReasonChanged(QString blockReason);

private:
    QString m_userName;
    QString m_name;
    QString m_status;
    QString m_imageSource;
    bool m_blocks;
    bool m_follows;
    QString m_loggedInUser;
    bool m_blockedByUserOfProfile;
    QString m_blockReason;
};
