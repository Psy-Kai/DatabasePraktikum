import QtQuick 2.4

Item {
    id: root

    signal postBabble(var text)

    BabbleCreateForm {
        id: form
        anchors.fill: parent

        buttonPostBabble.onClicked: {
            var text = textAreaBabbleText.text;
            if ((text.length < 1) || 280 < (text.length)) {
                label.visible = true;
                return;
            }

            root.postBabble(text);
        }

        textAreaBabbleText.onTextChanged: label.visible = false;
    }
}
