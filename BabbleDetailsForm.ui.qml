import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
    width: 400
    height: 400
    property alias babble: babble
    property alias buttonDelete: buttonDelete
    property alias buttonRebabble: buttonRebabble
    property alias buttonDislike: buttonDislike
    property alias buttonLike: buttonLike

    ColumnLayout {
        id: columnLayout
        height: 100
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top

        Babble {
            id: babble
            Layout.maximumWidth: 800
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
        }
        RowLayout {
            id: rowLayout
            width: 100
            height: 100
            Layout.maximumWidth: 800
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Button {
                id: buttonLike
                width: 85
                text: qsTr("Like")
                checkable: true
            }

            Button {
                id: buttonDislike
                text: qsTr("Dislike")
                checkable: true
            }

            Button {
                id: buttonRebabble
                text: qsTr("Rebabble")
                checkable: true
            }

            Item {
                height: buttonDelete.height
                Layout.fillWidth: true

                Button {
                    id: buttonDelete
                    text: qsTr("Delete Babble")
                    anchors.right: parent.right
                }
            }
        }
    }
}
