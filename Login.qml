import QtQuick 2.4

Item {
    id: root

    signal login(var user)

    function checkLoginCredentials() {
        if (checkUserCredentials(form.textFieldUsername.text, form.textFieldPassword.text))
            login(form.textFieldUsername.text);
        else
            form.labelInvalidCredentials.visible = true;
    }

    LoginForm {
        id: form
        anchors.fill: parent

        buttonLogin.onClicked: {
            checkLoginCredentials();
        }
        textFieldUsername.onTextChanged: {
            labelInvalidCredentials.visible = false;
        }
        textFieldUsername.onAccepted: {
            checkLoginCredentials();
        }
        textFieldPassword.onTextChanged: {
            labelInvalidCredentials.visible = false;
        }
        textFieldPassword.onAccepted: {
            checkLoginCredentials();
        }
    }
}
