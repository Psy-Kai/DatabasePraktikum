import QtQuick 2.4
import kd.components 1.0

Item {
    id: root

    signal userClicked(var user)
    signal showBabble(var babbleId)

    BabbleSearchForm {
        id: form

        anchors.fill: parent

        buttonSearchBabble.onClicked:
            model.searchString = textFieldSearchbar.text

        textFieldSearchbar.onAccepted:
            model.searchString = textFieldSearchbar.text

        listView.delegate: TimelineDelegate {
            anchors.left: parent.left
            anchors.right: parent.right

            babble.id: model.Id
            babble.user: model.User
            babble.name: model.Name
            babble.text: model.Text
            babble.likes: model.Likes
            babble.dislikes: model.Dislikes
            babble.rebabbles: model.Rebabbles
            babble.timestamp: model.Timestamp

            onUserClicked: root.userClicked(babble.user);
            onTextClicked: root.showBabble(babble.id);
        }

        listView.model: SearchModel {
            id: model
        }
    }
}
