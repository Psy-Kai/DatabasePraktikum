#pragma once

#include <QObject>
#include "databasefunctions.h"

class BabbleDetailsModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString loggedInUser READ loggedInUser WRITE setLoggedInUser NOTIFY loggedInUserChanged)
    Q_PROPERTY(Babble* babble READ babble WRITE setBabble NOTIFY babbleChanged)
    Q_PROPERTY(DisLikes disLikes READ disLikes WRITE setDisLikes NOTIFY disLikesChanged)
    Q_PROPERTY(bool rebabbles READ rebabbles WRITE setRebabbles NOTIFY rebabblesChanged)
public:
    enum DisLikes {
        None = 0,
        Like,
        Dislike
    };
    Q_ENUM(DisLikes)
    explicit BabbleDetailsModel();
    Q_INVOKABLE void queryBabble(qint32 babbleId);
    const Babble *babble() const;
    Babble *babble();
    bool rebabbles() const;
    QString loggedInUser() const;
    DisLikes disLikes() const;
public slots:
    void setBabble(Babble *babble);
    void setRebabbles(bool rebabbles);
    void setLoggedInUser(QString loggedInUser);
    void setDisLikes(DisLikes disLikes);
signals:
    void babbleChanged(Babble *babble);
    void rebabblesChanged(bool rebabbles);
    void loggedInUserChanged(QString loggedInUser);
    void disLikesChanged(DisLikes disLikes);
private:
    Babble m_babble;
    DisLikes m_disLikes;
    bool m_rebabbles;
    QString m_loggedInUser;
};
