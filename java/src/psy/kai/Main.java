package psy.kai;

import java.sql.Connection;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        Database db = new Database();
        if (!db.init())
            return;
        Connection conn = db.connection();
        SqlQuery query = new SqlQuery(conn);
        query.prepare("INSERT INTO dbp86.BabbleUser (name, userName, password) " +
        "VALUES ('Klaus', 'K.laus', '1234Pass')");
        if (query.executeUpdate() < 1)
            return;
        try {
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        while (query.next()) {
            System.out.println(query.valueString("username"));
        }
    }
}