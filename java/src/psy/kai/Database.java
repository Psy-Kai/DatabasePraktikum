package psy.kai;

import com.ibm.db2.jcc.DB2BaseDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {
    public boolean init() {
        try {
            Class.forName("com.ibm.db2.jcc.DB2Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Connection connection() {
        Properties properties = new Properties();
        properties.setProperty("securityMechanism", Integer.toString(DB2BaseDataSource.CLEAR_TEXT_PASSWORD_SECURITY));
        properties.setProperty("user","dbp86");
        properties.setProperty("password","ohpahf7r");

        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:db2://ophion.is.inf.uni-due.de:50086/kdbabble:currentSchema=dbp86;", properties);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }
}
