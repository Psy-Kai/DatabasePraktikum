package psy.kai;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlQuery {
    private Connection m_connection;
    private PreparedStatement m_preparedStatement;
    private int m_addBindValuePos;
    private ResultSet m_resultSet;

    public SqlQuery(Connection connection) {
        m_connection = connection;
        m_preparedStatement = null;
        m_addBindValuePos = 0;
        m_resultSet = null;
    }

    public boolean prepare(String statement) {
//        System.out.println("Statement to prepate: " + statement);
        try {
            m_preparedStatement = m_connection.prepareStatement(statement);
            m_addBindValuePos = 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void addBindValueString(String value) {
        if (m_preparedStatement == null)
            return;

        try {
            m_addBindValuePos++;
            m_preparedStatement.setString(m_addBindValuePos, value);
//            System.out.println("Bound value " + value + " at pos " + m_addBindValuePos);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addBindValueInt(int value) {
        if (m_preparedStatement == null)
            return;

        try {
            m_addBindValuePos++;
            m_preparedStatement.setInt(m_addBindValuePos, value);
//            System.out.println("Bound value " + value + " at pos " + m_addBindValuePos);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addBindValueBoolean(boolean value) {
        if (m_preparedStatement == null)
            return;

        try {
            m_addBindValuePos++;
            m_preparedStatement.setBoolean(m_addBindValuePos, value);
//            System.out.println("Bound value " + value + " at pos " + m_addBindValuePos);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean executeQuery() {
        if (m_preparedStatement == null)
            return false;

        try {
            m_resultSet = m_preparedStatement.executeQuery();
//            System.out.println("Result of query: " + m_resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public int executeUpdate() {
        if (m_preparedStatement == null)
            return -1;

        int result;
        try {
            result = m_preparedStatement.executeUpdate();
//            System.out.println("Result of update: " + result);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
        return result;
    }

    public boolean next() {
        if (m_resultSet == null)
            return false;

        boolean result;
        try {
            result = m_resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return result;
    }

    public String valueString(String field) {
        if (m_resultSet == null)
            return "";

        String result;
        try {
            result = m_resultSet.getString(field);
//            System.out.println("Value of column " + field + " is " + result);
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }

        return result;
    }

    public String valueInt(String field) {
        if (m_resultSet == null)
            return "";

        int result;
        try {
            result = m_resultSet.getInt(field);
//            System.out.println("Value of column " + field + " is " + result);
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }

        return Integer.toString(result);
    }
}
