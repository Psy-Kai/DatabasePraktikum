#pragma once

#include <QAbstractListModel>
#include "databasefunctions.h"

class DatabaseFunctionsInterface;
class SearchModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString searchString READ searchString WRITE setSearchString NOTIFY searchStringChanged)
public:
    enum Role {
        Id = Qt::UserRole + 1,
        User,
        Name,
        Text,
        Likes,
        Dislikes,
        Rebabbles,
        Timestamp
    };
    Q_ENUM(Role)
    explicit SearchModel();
    ~SearchModel() Q_DECL_OVERRIDE;
    QString searchString() const;
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
public slots:
    void setSearchString(QString searchString);
signals:
    void searchStringChanged(QString searchString);
private:
    QString m_searchString;
    DatabaseFunctionsInterface &m_databaseFunctions;
    QVector<Babble> m_data;
};
