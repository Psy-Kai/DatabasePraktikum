#include "databasefunctions.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QSqlError>
#include <QSqlRecord>
#include <QStandardPaths>
#include <QTemporaryFile>

Q_GLOBAL_STATIC(DATABASETYPE, kDatabaseInstance)

Babble::Babble() :
    id(-1)
  , likes(-1)
  , dislikes(-1)
  , rebabbles(-1)
{}

Babble::Babble(const Babble &other) :
    QObject()
  , id(other.id)
  , user(other.user)
  , name(other.name)
  , text(other.text)
  , likes(other.likes)
  , dislikes(other.dislikes)
  , rebabbles(other.rebabbles)
  , timestamp(other.timestamp)
{}

Babble::Babble(Babble &&other) :
    QObject()
  , id(qMove(other.id))
  , user(qMove(other.user))
  , name(qMove(other.name))
  , text(qMove(other.text))
  , likes(qMove(other.likes))
  , dislikes(qMove(other.dislikes))
  , rebabbles(qMove(other.rebabbles))
  , timestamp(qMove(other.timestamp))
{}

Babble &Babble::operator=(const Babble &other)
{
    if (this == &other)
        return *this;

    Babble tmp(other);
    *this = qMove(tmp);
    return *this;
}

Babble &Babble::operator=(Babble &&other)
{
    if (this == &other)
        return *this;

    id = qMove(other.id);
    user = qMove(other.user);
    name = qMove(other.name);
    text = qMove(other.text);
    likes = qMove(other.likes);
    dislikes = qMove(other.dislikes);
    rebabbles = qMove(other.rebabbles);
    timestamp = qMove(other.timestamp);
    return *this;
}

Babble::~Babble()
{}

bool operator==(const Babble &lhs, const Babble &rhs)
{
    return (lhs.id == rhs.id) &&
            (lhs.user == rhs.user) &&
            (lhs.name == rhs.name) &&
            (lhs.text == rhs.text) &&
            (lhs.likes == rhs.likes) &&
            (lhs.dislikes == rhs.dislikes) &&
            (lhs.rebabbles == rhs.rebabbles) &&
            (lhs.timestamp == rhs.timestamp);
}

bool operator!=(const Babble &lhs, const Babble &rhs)
{
    return !(lhs == rhs);
}

DatabaseFunctionsInterface &DatabaseFunctionsInterface::instance()
{
    return *kDatabaseInstance;
}

DatabaseFunctionsInterface::~DatabaseFunctionsInterface()
{}

SqliteFunctions::~SqliteFunctions()
{}

bool SqliteFunctions::init()
{
    QFile databaseFile(QStringLiteral(":/TestDatabase.db"));
    if (!databaseFile.open(QIODevice::ReadOnly))
        return false;

    auto *tempDatabaseFile = new QTemporaryFile(QCoreApplication::instance());
    if (!tempDatabaseFile->open())
        return false;

    tempDatabaseFile->write(databaseFile.readAll());

    auto &&database = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"));
    database.setDatabaseName(QFileInfo(*tempDatabaseFile).absoluteFilePath());
    return database.open();
}

QVariant SqliteFunctions::likesCount(const QVariant &babbleId) const
{
    return likesDislikesCount(babbleId, true);
}

QVariant SqliteFunctions::dislikesCount(const QVariant &babbleId) const
{
    return likesDislikesCount(babbleId, false);
}

QVariant SqliteFunctions::rebabbledCount(const QVariant &babbleId) const
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(Babble.id) "
                  "FROM Babble "
                  "JOIN BabbleUserRebabblesBabble ON Babble.id = BabbleUserRebabblesBabble.babbleId "
                  "WHERE Babble.id = ?");
    query.addBindValue(babbleId);

    if (!query.exec())
        return QVariant();
    query.next();
    return query.value(0);
}

QVector<Interaction> SqliteFunctions::babbles(const QString &user) const
{
    QSqlQuery query;
    query.prepare("SELECT * "
                  "FROM Babbles "
                  "WHERE interactionUser = :user "
                  "ORDER BY interactionDate DESC");
    query.bindValue(QStringLiteral(":user"), user);

    QVector<Interaction> result;
    if (!query.exec())
        return result;

    while (query.next()) {
        Interaction newInteraction;
        newInteraction.user = user;
        auto &babble = newInteraction.babble;
        babble.id = query.value("babbleId").value<qint32>();
        newInteraction.type = query.value("interactionType").value<QString>();
        newInteraction.timestamp = query.value("interactionDate").value<QString>();
        babble.user = query.value("babbleUserName").value<QString>();
        babble.name = query.value("babbleName").value<QString>();
        babble.text = query.value("babbleText").value<QString>();
        babble.likes = likesCount(babble.id).value<qint32>();
        babble.dislikes = dislikesCount(babble.id).value<qint32>();
        babble.rebabbles = rebabbledCount(babble.id).value<qint32>();
        babble.timestamp = query.value("babbleDate").value<QString>();
        result << newInteraction;
    }

    return result;
}

QVariant SqliteFunctions::babblesCount(const QString &user) const
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) "
                  "FROM Babbles "
                  "WHERE interactionUser = :user "
                  "ORDER BY interactionDate DESC");
    query.bindValue(QStringLiteral(":user"), user);

    if (!query.exec())
        return QVariant();
    query.next();
    return query.value(0);
}

UserProfile SqliteFunctions::userProfile(const QString &user) const
{
    QSqlQuery query;
    query.prepare("SELECT name, status, picture "
                  "FROM BabbleUser "
                  "WHERE userName = ?");
    query.addBindValue(user);

    UserProfile result;
    if (!query.exec())
        return result;

    if (!query.next())
        return result;

    result.user = user;
    result.name = query.value(0).value<QString>();
    result.status = query.value(1).value<QString>();
    result.image = query.value(2).value<QImage>();
    return result;
}

bool SqliteFunctions::checkUserCredentials(const QString &user, const QString &password) const
{
    QSqlQuery query;
    query.prepare("SELECT BabbleUser.userName "
                  "FROM BabbleUser "
                  "WHERE BabbleUser.userName = ? AND BabbleUser.password = ?");
    query.addBindValue(user);
    query.addBindValue(password);

    if (!query.exec())
        return false;

    if (!query.next())
        return false;

    return query.value(0) == user;
}

QVector<Babble> SqliteFunctions::searchBabbles(const QString &searchstring) const
{
    QVector<Babble> result;
    if (searchstring.isEmpty())
        return result;

    QSqlQuery query;
    query.prepare("SELECT * "
                  "FROM Babbles "
                  "WHERE babbleText LIKE ? AND interactionType = \"\"");
    query.addBindValue(QLatin1Char('%') + searchstring + QLatin1Char('%'));

    if (!query.exec())
        return result;

    while (query.next()) {
        Babble newBabble;
        newBabble.id = query.value("babbleId").value<qint32>();
        newBabble.user = query.value("babbleUserName").value<QString>();
        newBabble.name = query.value("babbleName").value<QString>();
        newBabble.text = query.value("babbleText").value<QString>();
        newBabble.likes = likesCount(newBabble.id).value<qint32>();
        newBabble.dislikes = dislikesCount(newBabble.id).value<qint32>();
        newBabble.rebabbles = rebabbledCount(newBabble.id).value<qint32>();
        newBabble.timestamp = query.value("babbleDate").value<QString>();
        result << newBabble;
    }

    return result;
}

bool SqliteFunctions::postBabble(const QString &user, const QString &text) const
{
    QSqlQuery query;
    query.prepare("INSERT INTO Babble (text, date, createdByUser) "
                  "VALUES (?, datetime('now'), ("
                    "SELECT BabbleUser.id "
                    "FROM BabbleUser "
                    "WHERE BabbleUser.userName = ?"
                  "))");
    query.addBindValue(text);
    query.addBindValue(user);
    return query.exec();
}

bool SqliteFunctions::deleteBabble(const QVariant &babbleId) const
{
    QSqlQuery query;
    query.prepare("DELETE FROM Babble "
                  "WHERE Babble.id = ?");
    query.addBindValue(babbleId);
    return query.exec();
}

Babble SqliteFunctions::babble(qint32 babbleId) const
{
    QSqlQuery query;
    query.prepare("SELECT * "
                  "FROM Babbles "
                  "WHERE babbleId = ? AND interactionType = \"\"");
    query.addBindValue(babbleId);

    Babble result;
    if (!query.exec())
        return result;
    if (!query.next())
        return result;

    result.id = query.value("babbleId").value<qint32>();
    result.user = query.value("babbleUserName").value<QString>();
    result.name = query.value("babbleName").value<QString>();
    result.text = query.value("babbleText").value<QString>();
    result.likes = likesCount(result.id).value<qint32>();
    result.dislikes = dislikesCount(result.id).value<qint32>();
    result.rebabbles = rebabbledCount(result.id).value<qint32>();
    result.timestamp = query.value("babbleDate").value<QString>();
    return result;
}

QVariant SqliteFunctions::likesBabble(const QString &user, qint32 babbleId) const
{
    QSqlQuery query;
    query.prepare("SELECT isLike "
                  "FROM BabbleUserLikesBabble "
                  "WHERE babbleUserId = "
                  "(SELECT id FROM BabbleUser WHERE userName = ?) "
                  "AND babbleId = ?");
    query.addBindValue(user);
    query.addBindValue(babbleId);

    if (!query.exec())
        return QVariant();
    if (!query.next())
        return QVariant();
    return query.value(0);
}

bool SqliteFunctions::likeBabble(const QString &user, qint32 babbleId, bool like, bool update) const
{
    QSqlQuery query;
    if (update)
        query.prepare("UPDATE BabbleUserLikesBabble "
                      "SET isLike = :isLike "
                      "WHERE babbleUserId = ("
                      "SELECT BabbleUser.id "
                      "FROM BabbleUser "
                      "WHERE BabbleUser.userName = :user) "
                      "AND babbleId = :babbleId");
    else
        query.prepare("INSERT INTO BabbleUserLikesBabble (babbleUserId, babbleId, isLike, date) "
                      "VALUES (("
                        "SELECT BabbleUser.id "
                        "FROM BabbleUser "
                        "WHERE BabbleUser.userName = :user), "
                      ":babbleId, :isLike, datetime('now'))");
    query.bindValue(QStringLiteral(":user"), user);
    query.bindValue(QStringLiteral(":babbleId"), babbleId);
    query.bindValue(QStringLiteral(":isLike"), like);
    return query.exec();
}

bool SqliteFunctions::unlikeBabble(const QString &user, qint32 babbleId) const
{
    QSqlQuery query;
    query.prepare("DELETE FROM BabbleUserLikesBabble "
                  "WHERE BabbleUserLikesBabble.babbleUserId = ("
                    "SELECT BabbleUser.id "
                    "FROM BabbleUser "
                    "WHERE BabbleUser.userName = ?"
                  ") AND BabbleUserLikesBabble.babbleId = ?");
    query.addBindValue(user);
    query.addBindValue(babbleId);
    return query.exec();
}

QVariant SqliteFunctions::wasRebabbled(const QString &user, qint32 babbleId) const
{
    QSqlQuery query;
    query.prepare("SELECT * "
                  "FROM BabbleUserRebabblesBabble "
                  "WHERE babbleUserId = "
                  "(SELECT id FROM BabbleUser WHERE userName = ?) "
                  "AND babbleId = ?");
    query.addBindValue(user);
    query.addBindValue(babbleId);

    if (!query.exec())
        return QVariant();
    if (!query.next())
        return false;
    return true;
}

bool SqliteFunctions::rebabble(const QString &user, qint32 babbleId) const
{
    QSqlQuery query;
    query.prepare("INSERT INTO BabbleUserRebabblesBabble (babbleUserId, babbleId, date) "
                  "VALUES (("
                    "SELECT BabbleUser.id "
                    "FROM BabbleUser "
                    "WHERE BabbleUser.userName = ?), "
                  "?, datetime('now'))");
    query.addBindValue(user);
    query.addBindValue(babbleId);
    return query.exec();
}

bool SqliteFunctions::unrebabble(const QString &user, qint32 babbleId) const
{
    QSqlQuery query;
    query.prepare("DELETE FROM BabbleUserRebabblesBabble "
                  "WHERE BabbleUserRebabblesBabble.babbleUserId = ("
                    "SELECT BabbleUser.id "
                    "FROM BabbleUser "
                    "WHERE BabbleUser.userName = ?"
                  ") AND BabbleUserRebabblesBabble.babbleId = ?");
    query.addBindValue(user);
    query.addBindValue(babbleId);
    return query.exec();
}

QPair<bool, QVariant> SqliteFunctions::isBlocked(const QString &blockingUser, const QString &blockedUser) const
{
    QSqlQuery query;
    query.prepare("SELECT reason "
                  "FROM BabbleUserBlocksBabbleUser "
                  "WHERE blockingBabbleUserId = "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?)"
                  "AND blockedBabbleUserId = "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?)");
    query.addBindValue(blockingUser);
    query.addBindValue(blockedUser);

    QPair<bool, QVariant> result{false, QVariant()};
    if (!query.exec())
        return result;
    result.first = true;
    if (!query.next())
        return result;
    result.second = query.value(0);
    return result;
}

bool SqliteFunctions::block(const QString &user, const QString &userToBlock, const QString &reason) const
{
    QSqlQuery query;
    query.prepare("INSERT INTO BabbleUserBlocksBabbleUser "
                  "(blockingBabbleUserId, blockedBabbleUserId, reason) "
                  "VALUES ("
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?), "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?), "
                  "?)");
    query.addBindValue(user);
    query.addBindValue(userToBlock);
    query.addBindValue(reason);
    return query.exec();
}

bool SqliteFunctions::unblock(const QString &user, const QString &userToUnblock) const
{
    QSqlQuery query;
    query.prepare("DELETE "
                  "FROM BabbleUserBlocksBabbleUser "
                  "WHERE blockingBabbleUserId = "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?)"
                  "AND blockedBabbleUserId = "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?)");
    query.addBindValue(user);
    query.addBindValue(userToUnblock);
    return query.exec();
}

QVariant SqliteFunctions::follows(const QString &followingUser, const QString &followedUser) const
{
    QSqlQuery query;
    query.prepare("SELECT * "
                  "FROM BabbleUserFollowsBabbleUser "
                  "WHERE followingBabbleUserId = "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?)"
                  "AND followedBabbleUserId = "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?)");
    query.addBindValue(followingUser);
    query.addBindValue(followedUser);

    if (!query.exec())
        return QVariant();
    if (query.next())
        return true;
    return false;
}

bool SqliteFunctions::follow(const QString &user, const QString &userToFollow) const
{
    QSqlQuery query;
    query.prepare("INSERT INTO BabbleUserFollowsBabbleUser "
                  "(followingBabbleUserId, followedBabbleUserId) "
                  "VALUES ("
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?), "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?))");
    query.addBindValue(user);
    query.addBindValue(userToFollow);
    return query.exec();
}

bool SqliteFunctions::unfollow(const QString &user, const QString &userToUnfollow) const
{
    QSqlQuery query;
    query.prepare("DELETE "
                  "FROM BabbleUserFollowsBabbleUser "
                  "WHERE followingBabbleUserId = "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?)"
                  "AND followedBabbleUserId = "
                  "(SELECT BabbleUser.id FROM BabbleUser WHERE BabbleUser.userName = ?)");
    query.addBindValue(user);
    query.addBindValue(userToUnfollow);
    return query.exec();
}

QVariant SqliteFunctions::likesDislikesCount(const QVariant &babbleId, bool like) const
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(Babble.id) "
                  "FROM Babble "
                  "JOIN BabbleUserLikesBabble ON Babble.id = BabbleUserLikesBabble.babbleId "
                  "WHERE Babble.id = ? AND BabbleUserLikesBabble.isLike = ?");
    query.addBindValue(babbleId);
    query.addBindValue(like);

    if (!query.exec())
        return QVariant();
    query.next();
    return query.value(0);
}

DB2JavaFunctions::DB2JavaFunctions() :
    m_java()
  , m_database(m_java)
{}

bool DB2JavaFunctions::init()
{
    auto createClassFile = [](const QDir &dir, const QString &className) -> bool {
        QFile qrcFile(QStringLiteral(":/java/out/production/DBPraktikum/%1.class").arg(className));
        if (!qrcFile.open(QIODevice::ReadOnly))
            return false;

        QFileInfo fileInfo(dir.filePath(className) + QLatin1String(".class"));
        dir.mkpath(fileInfo.absolutePath());
        QFile tmpFile(fileInfo.absoluteFilePath());
        if (!tmpFile.open(QIODevice::WriteOnly))
            return false;

        tmpFile.write(qrcFile.readAll());
        return true;
    };

    QDir classDirectory(QStandardPaths::writableLocation(QStandardPaths::TempLocation));
    classDirectory = classDirectory.filePath(QCoreApplication::applicationName());
    classDirectory.mkpath(classDirectory.absolutePath());

    if (!createClassFile(classDirectory, "psy/kai/Database"))
        return false;
    if (!createClassFile(classDirectory, "psy/kai/SqlQuery"))
        return false;

    if (!m_java.startVm({ classDirectory.absolutePath(), DB2DRIVERPATH }))
        return false;
    if (!m_database.initJavaObject())
        return false;
    return m_database.init();
}

QVariant DB2JavaFunctions::likesCount(const QVariant &babbleId) const
{
    return likesDislikesCount(babbleId, true);
}

QVariant DB2JavaFunctions::dislikesCount(const QVariant &babbleId) const
{
    return likesDislikesCount(babbleId, false);
}

QVariant DB2JavaFunctions::rebabbledCount(const QVariant &babbleId) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return QVariant();

    query.prepare("SELECT COUNT(dbp86.Babble.id) AS rebabbledCount "
                  "FROM dbp86.Babble "
                  "JOIN dbp86.BabbleUserRebabblesBabble ON dbp86.Babble.id = dbp86.BabbleUserRebabblesBabble.babbleId "
                  "WHERE dbp86.Babble.id = ?");
    query.addBindValueInt(babbleId.value<int>());

    if (!query.executeQuery())
        return QVariant();
    if (!query.next())
        return QVariant();
    return query.valueInt("rebabbledCount");
}

QVector<Interaction> DB2JavaFunctions::babbles(const QString &user) const
{
    QVector<Interaction> result;
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return result;

    query.prepare("SELECT * "
                  "FROM dbp86.Babbles "
                  "WHERE interactionUser = ? "
                  "ORDER BY interactionDate DESC");
    query.addBindValueString(user);

    if (!query.executeQuery())
        return result;

    while (query.next()) {
        Interaction newInteraction;
        newInteraction.user = user;
        auto &babble = newInteraction.babble;
        babble.id = query.valueInt("babbleId").value<qint32>();
        newInteraction.type = query.valueString("interactionType").value<QString>();
        newInteraction.timestamp = query.valueString("interactionDate").value<QString>();
        babble.user = query.valueString("babbleUserName").value<QString>();
        babble.name = query.valueString("babbleName").value<QString>();
        babble.text = query.valueString("babbleText").value<QString>();
        babble.likes = likesCount(babble.id).value<qint32>();
        babble.dislikes = dislikesCount(babble.id).value<qint32>();
        babble.rebabbles = rebabbledCount(babble.id).value<qint32>();
        babble.timestamp = query.valueString("babbleDate").value<QString>();
        result << newInteraction;
    }

    return result;
}

QVariant DB2JavaFunctions::babblesCount(const QString &user) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return QVariant();

    query.prepare("SELECT COUNT(*) AS babblesCount "
                  "FROM dbp86.Babbles "
                  "WHERE interactionUser = ?");
    query.addBindValueString(user);

    if (!query.executeQuery())
        return QVariant();
    if (!query.next())
        return QVariant();
    return query.valueInt("babblesCount");
}

UserProfile DB2JavaFunctions::userProfile(const QString &user) const
{
    UserProfile result;
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return result;

    query.prepare("SELECT name, status, picture "
                  "FROM dbp86.BabbleUser "
                  "WHERE userName = ?");
    query.addBindValueString(user);

    if (!query.executeQuery())
        return result;

    if (!query.next())
        return result;

    result.user = user;
    result.name = query.valueString("name").value<QString>();
    result.status = query.valueString("status").value<QString>();
    result.image = QImage();
    return result;
}

bool DB2JavaFunctions::checkUserCredentials(const QString &user, const QString &password) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("SELECT dbp86.BabbleUser.userName "
                  "FROM dbp86.BabbleUser "
                  "WHERE dbp86.BabbleUser.userName = ? AND dbp86.BabbleUser.password = ?");
    query.addBindValueString(user);
    query.addBindValueString(password);

    if (!query.executeQuery())
        return false;

    if (!query.next())
        return false;

    return query.valueString("userName").value<QString>() == user;
}

QVector<Babble> DB2JavaFunctions::searchBabbles(const QString &searchstring) const
{
    QVector<Babble> result;
    if (searchstring.isEmpty())
        return result;

    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return result;

    query.prepare("SELECT * "
                  "FROM dbp86.Babbles "
                  "WHERE babbleText LIKE ? AND interactionType = ''");
    query.addBindValueString(QLatin1Char('%') + searchstring + QLatin1Char('%'));

    if (!query.executeQuery())
        return result;

    while (query.next()) {
        Babble newBabble;
        newBabble.id = query.valueInt("babbleId").value<qint32>();
        newBabble.user = query.valueString("babbleUserName").value<QString>();
        newBabble.name = query.valueString("babbleName").value<QString>();
        newBabble.text = query.valueString("babbleText").value<QString>();
        newBabble.likes = likesCount(newBabble.id).value<qint32>();
        newBabble.dislikes = dislikesCount(newBabble.id).value<qint32>();
        newBabble.rebabbles = rebabbledCount(newBabble.id).value<qint32>();
        newBabble.timestamp = query.valueString("babbleDate").value<QString>();
        result << newBabble;
    }

    return result;
}

bool DB2JavaFunctions::postBabble(const QString &user, const QString &text) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("INSERT INTO dbp86.Babble (text, date, createdByUser) "
                  "VALUES (?, CURRENT TIMESTAMP, ("
                    "SELECT dbp86.BabbleUser.id "
                    "FROM dbp86.BabbleUser "
                    "WHERE dbp86.BabbleUser.userName = ?"
                  "))");
    query.addBindValueString(text);
    query.addBindValueString(user);

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

bool DB2JavaFunctions::deleteBabble(const QVariant &babbleId) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("DELETE FROM dbp86.Babble "
                  "WHERE dbp86.Babble.id = ?");
    query.addBindValueInt(babbleId.value<int>());

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

Babble DB2JavaFunctions::babble(qint32 babbleId) const
{
    Babble result;
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return result;

    query.prepare("SELECT * "
                  "FROM dbp86.Babbles "
                  "WHERE babbleId = ? AND interactionType = ''");
    query.addBindValueInt(babbleId);

    if (!query.executeQuery())
        return result;
    if (!query.next())
        return result;

    result.id = query.valueInt("babbleId").value<qint32>();
    result.user = query.valueString("babbleUserName").value<QString>();
    result.name = query.valueString("babbleName").value<QString>();
    result.text = query.valueString("babbleText").value<QString>();
    result.likes = likesCount(result.id).value<qint32>();
    result.dislikes = dislikesCount(result.id).value<qint32>();
    result.rebabbles = rebabbledCount(result.id).value<qint32>();
    result.timestamp = query.valueString("babbleDate").value<QString>();
    return result;
}

QVariant DB2JavaFunctions::likesBabble(const QString &user, qint32 babbleId) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return QVariant();

    query.prepare("SELECT isLike "
                  "FROM dbp86.BabbleUserLikesBabble "
                  "WHERE babbleUserId = "
                  "(SELECT id FROM dbp86.BabbleUser WHERE userName = ?) "
                  "AND babbleId = ?");
    query.addBindValueString(user);
    query.addBindValueInt(babbleId);

    if (!query.executeQuery())
        return QVariant();
    if (!query.next())
        return QVariant();
    return query.valueInt("isLike");
}

bool DB2JavaFunctions::likeBabble(const QString &user, qint32 babbleId, bool like, bool update) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    if (update) {
        query.prepare("UPDATE dbp86.BabbleUserLikesBabble "
                      "SET isLike = ? "
                      "WHERE babbleUserId = ("
                      "SELECT dbp86.BabbleUser.id "
                      "FROM dbp86.BabbleUser "
                      "WHERE dbp86.BabbleUser.userName = ?) "
                      "AND babbleId = ?");
        query.addBindValueBoolean(like);
        query.addBindValueString(user);
        query.addBindValueInt(babbleId);
    } else {
        query.prepare("INSERT INTO dbp86.BabbleUserLikesBabble (babbleUserId, babbleId, isLike, date) "
                      "VALUES (("
                        "SELECT dbp86.BabbleUser.id "
                        "FROM dbp86.BabbleUser "
                        "WHERE dbp86.BabbleUser.userName = ?), "
                      "?, ?, CURRENT TIMESTAMP)");
        query.addBindValueString(user);
        query.addBindValueInt(babbleId);
        query.addBindValueBoolean(like);
    }

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

bool DB2JavaFunctions::unlikeBabble(const QString &user, qint32 babbleId) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("DELETE FROM dbp86.BabbleUserLikesBabble "
                  "WHERE dbp86.BabbleUserLikesBabble.babbleUserId = ("
                    "SELECT dbp86.BabbleUser.id "
                    "FROM dbp86.BabbleUser "
                    "WHERE dbp86.BabbleUser.userName = ?"
                  ") AND dbp86.BabbleUserLikesBabble.babbleId = ?");
    query.addBindValueString(user);
    query.addBindValueInt(babbleId);

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

QVariant DB2JavaFunctions::wasRebabbled(const QString &user, qint32 babbleId) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return QVariant();

    query.prepare("SELECT * "
                  "FROM dbp86.BabbleUserRebabblesBabble "
                  "WHERE babbleUserId = "
                  "(SELECT id FROM dbp86.BabbleUser WHERE userName = ?) "
                  "AND babbleId = ?");
    query.addBindValueString(user);
    query.addBindValueInt(babbleId);

    if (!query.executeQuery())
        return QVariant();
    if (!query.next())
        return false;
    return true;
}

bool DB2JavaFunctions::rebabble(const QString &user, qint32 babbleId) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("INSERT INTO dbp86.BabbleUserRebabblesBabble (babbleUserId, babbleId, date) "
                  "VALUES (("
                    "SELECT dbp86.BabbleUser.id "
                    "FROM dbp86.BabbleUser "
                    "WHERE dbp86.BabbleUser.userName = ?), "
                  "?, CURRENT TIMESTAMP)");
    query.addBindValueString(user);
    query.addBindValueInt(babbleId);

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

bool DB2JavaFunctions::unrebabble(const QString &user, qint32 babbleId) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("DELETE FROM dbp86.BabbleUserRebabblesBabble "
                  "WHERE dbp86.BabbleUserRebabblesBabble.babbleUserId = ("
                    "SELECT dbp86.BabbleUser.id "
                    "FROM dbp86.BabbleUser "
                    "WHERE dbp86.BabbleUser.userName = ?"
                  ") AND dbp86.BabbleUserRebabblesBabble.babbleId = ?");
    query.addBindValueString(user);
    query.addBindValueInt(babbleId);

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

QPair<bool, QVariant> DB2JavaFunctions::isBlocked(const QString &blockingUser, const QString &blockedUser) const
{
    QPair<bool, QVariant> result{false, QVariant()};
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return result;

    query.prepare("SELECT reason "
                  "FROM dbp86.BabbleUserBlocksBabbleUser "
                  "WHERE blockingBabbleUserId = "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?)"
                  "AND blockedBabbleUserId = "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?)");
    query.addBindValueString(blockingUser);
    query.addBindValueString(blockedUser);

    if (!query.executeQuery())
        return result;
    result.first = true;
    if (!query.next())
        return result;
    result.second = query.valueString("reason");
    return result;
}

bool DB2JavaFunctions::block(const QString &user, const QString &userToBlock, const QString &reason) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("INSERT INTO dbp86.BabbleUserBlocksBabbleUser "
                  "(blockingBabbleUserId, blockedBabbleUserId, reason) "
                  "VALUES ("
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?), "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?), "
                  "?)");
    query.addBindValueString(user);
    query.addBindValueString(userToBlock);
    query.addBindValueString(reason);

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

bool DB2JavaFunctions::unblock(const QString &user, const QString &userToUnblock) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("DELETE "
                  "FROM dbp86.BabbleUserBlocksBabbleUser "
                  "WHERE blockingBabbleUserId = "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?)"
                  "AND blockedBabbleUserId = "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?)");
    query.addBindValueString(user);
    query.addBindValueString(userToUnblock);

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

QVariant DB2JavaFunctions::follows(const QString &followingUser, const QString &followedUser) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return QVariant();

    query.prepare("SELECT * "
                  "FROM dbp86.BabbleUserFollowsBabbleUser "
                  "WHERE followingBabbleUserId = "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?)"
                  "AND followedBabbleUserId = "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?)");
    query.addBindValueString(followingUser);
    query.addBindValueString(followedUser);

    if (!query.executeQuery())
        return QVariant();
    if (query.next())
        return true;
    return false;
}

bool DB2JavaFunctions::follow(const QString &user, const QString &userToFollow) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("INSERT INTO dbp86.BabbleUserFollowsBabbleUser "
                  "(followingBabbleUserId, followedBabbleUserId) "
                  "VALUES ("
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?), "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?))");
    query.addBindValueString(user);
    query.addBindValueString(userToFollow);

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

bool DB2JavaFunctions::unfollow(const QString &user, const QString &userToUnfollow) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return false;

    query.prepare("DELETE "
                  "FROM dbp86.BabbleUserFollowsBabbleUser "
                  "WHERE followingBabbleUserId = "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?)"
                  "AND followedBabbleUserId = "
                  "(SELECT dbp86.BabbleUser.id FROM dbp86.BabbleUser WHERE dbp86.BabbleUser.userName = ?)");
    query.addBindValueString(user);
    query.addBindValueString(userToUnfollow);

    if (query.executeUpdate() < 1)
        return false;
    query.commit();
    return true;
}

QVariant DB2JavaFunctions::likesDislikesCount(const QVariant &babbleId, bool like) const
{
    JavaSqlQuery query(m_java, m_database.connection());
    if (!query.initJavaObject())
        return QVariant();

    query.prepare("SELECT COUNT(dbp86.Babble.id) AS likesDislikesCount "
                  "FROM dbp86.Babble "
                  "JOIN dbp86.BabbleUserLikesBabble ON dbp86.Babble.id = dbp86.BabbleUserLikesBabble.babbleId "
                  "WHERE dbp86.Babble.id = ? AND dbp86.BabbleUserLikesBabble.isLike = ?");
    query.addBindValueInt(babbleId.value<int>());
    query.addBindValueBoolean(like);

    if (!query.executeQuery())
        return QVariant();
    if (!query.next())
        return QVariant();
    return query.valueInt("likesDislikesCount");
}
