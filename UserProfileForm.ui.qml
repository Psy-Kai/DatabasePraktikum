import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import kd.components 1.0

Item {
    id: root
    width: 800
    height: 600
    property alias labelBlockingReason: labelBlockingReason
    property alias image: image
    property alias buttonNewBabble: buttonNewBabble
    property alias labelStatusValue: labelStatusValue
    property alias labelNameValue: labelNameValue
    property alias labelUserValue: labelUserValue
    property alias buttonBlock: buttonBlock
    property alias buttonFollow: buttonFollow
    property alias buttonSearchBabble: buttonSearchBabble
    property alias listView: listView

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        RowLayout {
            id: rowLayoutHeader
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.maximumWidth: 800
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top

            Item {
                id: itemButtonSearchBabbleDummy
                height: buttonSearchBabble.height
                Layout.fillWidth: true

                Button {
                    id: buttonSearchBabble
                    text: qsTr("Search Babble")
                    anchors.top: parent.top
                    anchors.left: parent.left
                }
            }

            Button {
                id: buttonFollow
                text: qsTr("Follow")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            Button {
                id: buttonBlock
                text: qsTr("Block")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
        }

        RowLayout {
            id: rowLayoutPersonalInfo
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.maximumWidth: 800

            GridLayout {
                id: gridLayoutPersonalInfo
                rowSpacing: 10
                columnSpacing: 5
                rows: 3
                columns: 2

                Label {
                    id: labelUser
                    text: qsTr("User:")
                }

                Label {
                    id: labelUserValue
                }

                Label {
                    id: labelName
                    text: qsTr("Name:")
                }

                Label {
                    id: labelNameValue
                    text: qsTr("")
                }

                Label {
                    id: labelStatus
                    text: qsTr("Status:")
                }

                Label {
                    id: labelStatusValue
                    text: qsTr("")
                }
            }

            Image {
                id: image
                width: height
                height: gridLayoutPersonalInfo.height
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                source: "qrc:/ProfilePicPlaceholder.png"
            }
        }

        Item {
            id: itemTimelineContainer
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.maximumWidth: 800
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Button {
                id: buttonNewBabble
                text: qsTr("New Babble")
                anchors.top: parent.top
                anchors.right: parent.right
            }

            ListView {
                id: listView
                boundsBehavior: Flickable.StopAtBounds
                spacing: 5
                clip: true
                anchors.topMargin: 3
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: buttonNewBabble.bottom
            }

            Label {
                id: labelBlockingReason
                color: "#ff0000"
                text: qsTr("You cannot see this Profile because the User blocked you.")
                visible: false
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: buttonNewBabble.bottom
                anchors.topMargin: 3
            }
        }
    }
}
